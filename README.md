<h1 align="center">Activity Summary</h1>
<p align="center">
    <a alt="Releases" href="https://gitlab.com/jan-di/activity-summary/-/releases"><img src="https://badgen.net/gitlab/release/jan-di/activity-summary"></a>
    <a alt="Pipelines" href="https://gitlab.com/jan-di/activity-summary/-/pipelines"><img src="https://gitlab.com/jan-di/activity-summary/badges/main/pipeline.svg"></a>
    <a alt="Open Issues" href="https://gitlab.com/jan-di/activity-summary/-/issues"><img src="https://badgen.net/gitlab/open-issues/jan-di/activity-summary"></a>
    <a alt="License" href="https://gitlab.com/jan-di/activity-summary/-/blob/main/LICENSE"><img src="https://badgen.net/gitlab/license/jan-di/activity-summary"></a>
</p>
<p align="center">
    <a alt="Production Environment" href="https://activity-summary.jand.one"><img src="https://badgen.net/badge/production/activity-summary.jand.one/grey"></a>
    <a alt="Staging Environment" href="https://activity-summary-stage.jand.one"><img src="https://badgen.net/badge/staging/activity-summary-stage.jand.one/grey"></a>
</p>

Webapplication to show a visualized summary of your activities on Strava