window.Popper = require('popper.js');
window.$ = window.jQuery = require('jquery');

var Highcharts = require('highcharts');
require('highcharts/modules/exporting')(Highcharts);
require('highcharts/modules/heatmap')(Highcharts);
require('bootstrap');

import { StartHeatmapChart } from './display/StartHeatmapChart';
import { ActivityTable } from './display/ActivitiesTable';
import { GearTable } from './display/GearTable';
import { GearChart } from './display/GearChart';
import { DeviceTable } from './display/DeviceTable';
import { DeviceChart } from './display/DeviceChart';
import { AnnualCumulativeChart } from './display/AnnualCumulativeChart';
import { SummaryTable } from './display/SummaryTable';

Highcharts.setOptions({
    credits: {
        enabled: false
    }
})

function handleFilterRadioChange(element, radioName, valueName) {
    let display = $(element).closest('.display')
    let value = $('input[name="' + radioName + '"]:checked', display).val()

    if (value.length != 0) {
        display.attr(valueName, value);
    } else {
        display.removeAttr(valueName);
    }
}

function handleFilterCheckboxChange(element, boxName, valueName) {
    let display = $(element).closest('.display')
    let boxes = $('input[name="' + boxName +'"]', display)
    let checkedBoxes = boxes.filter(':checked')

    if (boxes.length != checkedBoxes.length) {
        let values = [];
        checkedBoxes.each(function() {
            values.push($(this).val());
        })
        display.attr(valueName, values.join());
    } else {
        display.removeAttr(valueName);
    }
}

$(function() {
    // Display Visibility Filter
    $('input[name="display-filter-visibility"]').change(function() {
        handleFilterRadioChange(this, 'display-filter-visibility', 'data-filter-visibility');
    })

    // Display Commute Filter
    $('input[name="display-filter-commute"]').change(function() {
        handleFilterRadioChange(this, 'display-filter-commute', 'data-filter-commute');
    })

    // Display Types Filter
    $('input[name="display-filter-types"]').change(function() {
        handleFilterCheckboxChange(this, 'display-filter-types', 'data-filter-types');
    });

    // Display Years Filter
    $('input[name="display-filter-years"]').change(function() {
        handleFilterCheckboxChange(this, 'display-filter-years', 'data-filter-years');
    });

    // Enable Tooltips
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    // Enable Displays
    let displayHandler = {
        'summary-table': SummaryTable,
        'activity-table': ActivityTable,
        'gear-table': GearTable,
        'gear-chart': GearChart,
        'device-table': DeviceTable,
        'device-chart': DeviceChart,
        'annual-cumulative-chart': AnnualCumulativeChart,
        'start-heatmap-chart': StartHeatmapChart
    }
    Object.keys(displayHandler).forEach(key => {
        $(`.display.${key}`).each(function() {
            let display = new displayHandler[key](Highcharts, this);
            display.load();
        });
    });
});