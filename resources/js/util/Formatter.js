function formatSeconds(seconds) {
  let segments = [3600, 60]
    .reduceRight(
      (pipeline, breakpoint) => (remainder) =>
        [Math.floor(remainder / breakpoint)].concat(
          pipeline(remainder % breakpoint)
        ),
      (r) => [r]
    )(seconds)
    .map((amount) => amount.toString().padStart(2, "0"));

  if (segments[0] == "00") {
    segments[0] = `<span class="text-muted">${segments[0]}`;
    if (segments[1] == "00") {
      segments[2] = `</span>${segments[2]}`;
    } else {
      segments[1] = `</span>${segments[1]}`;
    }
  }

  return segments.join(":");
}

function formatKilometers(meters) {
    return formatMeters(meters / 1000);
}

function formatMeters(meters) {
    return (Math.round((meters + Number.EPSILON) * 100) / 100).toFixed(2).toString();
}

function cutText(text, limit) {
    if (text.length > limit) {
        text = text.slice(0, limit - 2) + '..';
    }
    return text
}

export { formatSeconds, formatKilometers, formatMeters, cutText };
