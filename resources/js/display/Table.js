import Display from "./Display";

class Table extends Display {
    render(data, global) {
        let html = `<table class="table table-hover slim">
                <thead>
                    <tr>${this.renderHeader(global)}</tr>
                </thead>
                <tbody>`;

        data.body.forEach((row, index) => {
            html += `<tr>${this.renderBody(row, index + 1, global)}</tr>`;
        });
        html += `</tbody></table>`

        if (data.body.length == 0) {
            html += `<div class="alert alert-info" role="alert"><i class="fas fa-question-circle"></i> No data to show</div>`
        }

        this.$containerElement.html(html);
    }

    // abstract
    renderHeader(global) {
        throw new Error('Method renderHeader must be implemented!');
    }

    // abstract
    renderBody(row, index, global) {
        throw new Error('Method renderBody must be implemented!');
    }

    afterRender() {
        $(function () {
            $('[data-toggle="tooltip"]', this.$containerElement).tooltip()
        })
    }
}

export { Table }