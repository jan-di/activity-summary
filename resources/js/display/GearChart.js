import { Chart } from "./Chart";

class GearChart extends Chart {
    fetchUrl() {
        return '/api/chart/gear';
    }

    getChartOptions(data) {
        return {
            chart: {
                type: 'pie',
                height: 300
            },

            series: [
                {
                    name: 'Activities',
                    data: data.series[0].data,
                }
            ],
        }
    }
}

export { GearChart }