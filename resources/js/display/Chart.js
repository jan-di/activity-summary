import Display from "./Display";

class Chart extends Display {
    render(data, global) {
        let options = this.getChartOptions(data, global);

        if (options.series.length == 0 || options.series[0].data.length == 0) {
            options.title = {
                useHTML: true,
                text: '<i class="fas fa-question-circle"></i> No data to show',
            }
        } else {
            options.title = {
                text: ''
            }
        }

        this.$containerElement.highcharts(options);
    }

    // abstract
    getChartOptions(data, global) {
        throw new Error('Method getChartOptions must be implemented!');
    }
}

export { Chart }