import { Table } from "./Table";
import { cutText, formatKilometers, formatMeters, formatSeconds } from "../util/Formatter";

class SummaryTable extends Table {
    fetchUrl() {
        return '/api/table/summary';
    }
    
    renderHeader() {
        return `
            <th scope="col" class="text-center">#</th>
            <th scope="col">Type</th>
            <th scope="col" class="text-right">Activities</th>
            <th scope="col" class="text-right">Total Moving Time</th>
            <th scope="col" class="text-right">Avg. Moving Time</th>
            <th scope="col" class="text-right">Total Distance <sup>km</sup></th>
            <th scope="col" class="text-right">Avg. Distance <sup>km</sup></th>
            <th scope="col" class="text-right">Total Elevation <sup>m</sup></th>
            <th scope="col" class="text-right">Avg. Elevation <sup>m</sup></th>
            <th scope="col" class="text-center">Commute</th>
            <th scope="col" class="text-center">Private</th>
        `;
    }

    renderBody(row, index, global) {

        let border = row.type ? '' : 'double-border'

        return `
            <th scope="row" class="text-center ${border}">${row.type ? index : '&rdsh;'}</th>
            <td class=" ${border}">${row.type ? cutText(row.type, 15) : '<span class="text-muted">&horbar;</span>'}</td>
            <td class="text-right ${border}">${row.total_activities}</td>
            <td class="text-right ${border}">${formatSeconds(row.total_moving_time)}</td>
            <td class="text-right ${border}">${formatSeconds(row.average_moving_time)}</td>
            <td class="text-right ${border}">${formatKilometers(row.total_distance)}</td>
            <td class="text-right ${border}">${formatKilometers(row.average_distance)}</td>
            <td class="text-right ${border}">${formatMeters(row.total_elevation)}</td>
            <td class="text-right ${border}">${formatMeters(row.average_elevation)}</td>
            <td class="text-center ${border}">${row.commute_activities}<sub>/${row.total_activities}</sub></td>
            <td class="text-center ${border}">${row.private_activities}<sub>/${row.total_activities}</sub></td>
        `
    }
}

export { SummaryTable }