export default class Display {
    constructor(highcharts, displayElement) {
        this.highcharts = highcharts
        this.$displayElement = $(displayElement)
        this.$spinnerElement = this.$displayElement.find('.spinner')
        this.$containerElement = this.$displayElement.find('.content')

        this.$displayElement.find('.dropdown').on('hide.bs.dropdown', function () {
            this.load();
        }.bind(this));
    }

    prepareUrl(url) {
        return new URL(url, document.location);
    }

    applyActivityFilters(params) {
        let athleteId = this.$displayElement.attr('data-athlete');
        params.append('athlete', athleteId);

        let visibilityFilter = this.$displayElement.attr('data-filter-visibility');
        if (visibilityFilter != undefined) {
            params.append('visibility', visibilityFilter)
        }

        let commuteFilter = this.$displayElement.attr('data-filter-commute');
        if (commuteFilter != undefined) {
            params.append('commute', commuteFilter)
        }

        let typesFilter = this.$displayElement.attr('data-filter-types');
        if (typesFilter != undefined) {
            params.append('types', typesFilter)
        }

        let yearsFilter = this.$displayElement.attr('data-filter-years');
        if (yearsFilter != undefined) {
            params.append('years', yearsFilter)
        }
    }

    applyFixedParams(params) {
        let fixedParams = this.$displayElement.attr('data-fixed-params');
        if (fixedParams != undefined) {
            for(let pair of new URLSearchParams(fixedParams).entries()) {
                params.append(pair[0], pair[1]);
            }
        }
    }

    load() {
        let url = this.prepareUrl(this.fetchUrl());
        this.applyActivityFilters(url.searchParams);
        this.applyFixedParams(url.searchParams);
        this.fetchAndRender(url);
    }

    fetchAndRender(url) {
        this.$spinnerElement.show();
        $.getJSON(url.href, function (response) {
            let { data, global } = response;

            this.beforeRender(global);
            this.render(data, global);
            this.afterRender(global);
        }.bind(this))
        .fail(function() {
            this.$containerElement.html('<div class="alert alert-warning" role="alert">Error while fetching data!</div>');
        }.bind(this))
        .always(function() {
            this.$spinnerElement.css("visibility", "hidden");
        }.bind(this));
    }

    // abstract
    fetchUrl() {
        throw new Error('Method fetchUrl must be implemented!');
    }

    // abstract
    beforeRender(global) {}

    // abstract
    render() {
        throw new Error('Method render must be implemented!');
    }

    // abstract
    afterRender(global) {}
}