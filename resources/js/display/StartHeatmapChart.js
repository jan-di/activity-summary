import { Chart } from "./Chart";

class StartHeatmapChart extends Chart {
    fetchUrl() {
        return '/api/chart/start-heatmap';
    }

    getChartOptions(data) {
        return {
            chart: {
                type: 'heatmap',
                marginTop: 40,
                marginBottom: 80,
                plotBorderWidth: 1
            },
        
            xAxis: {
                title: {
                    text: 'Hour'
                },
                categories: Array(24).fill(),
                min: 0,
                max: 23
            },
        
            yAxis: {
                categories: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                title: {
                    text: 'Weekday'
                },
                reversed: true
            },
        
            colorAxis: {
                min: 0,
                minColor: '#FFFFFF',
                maxColor: this.highcharts.getOptions().colors[0]
            },
        
            legend: {
                align: 'right',
                layout: 'vertical',
                margin: 0,
                verticalAlign: 'top',
                y: 25,
                symbolHeight: 280
            },

            series: [
                {
                    name: 'Activity Start Time',
                    borderWidth: 1,
                    data: data.series[0].data,
                    dataLabels: {
                        enabled: true,
                        color: '#000000'
                    }
                }
            ],

            tooltip: {
                formatter: function() {return `<b>${this.series.yAxis.categories[this.point.y]} ${this.point.x}:00</b>
                    <br/>Activities: ${this.point.value}`}
            },

            
        
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        yAxis: {
                            labels: {
                                formatter: function () {
                                    return this.value.charAt(0);
                                }
                            }
                        }
                    }
                }]
            }
        
        }
    }
}

export { StartHeatmapChart }