import { Table } from "./Table";
import { cutText, formatKilometers, formatMeters, formatSeconds } from "../util/Formatter";

class ActivityTable extends Table {
    fetchUrl() {
        return '/api/table/activities';
    }
    
    renderHeader() {
        return `
            <th scope="col" class="text-center">#</th>
            <th scope="col">Date</th>
            <th scope="col">Type</th>
            <th scope="col">Name</th>
            <th scope="col">Duration</th>
            <th scope="col">Dist. <sup>km</sup></th>
            <th scope="col">Elev. <sup>m</sup></th>
            <th scope="col">Private</th>
            <th scope="col">Commute</th>
            <th scope="col">Device</th>
            <th scope="col">Gear</th>
        `;
    }

    renderBody(row, index, global) {
        return `
            <th scope="row" class="text-center">${index}</th>
            <td>${row.local_start_date}</td>
            <td>${cutText(row.type, 10)}</td>
            <td>${cutText(row.name, 30)} 
                <a class="strava" target="_blank" href="${global.activity_url.replace('{id}', row.id)}" 
                    data-toggle="tooltip" title="View on Strava">
                <sup><i class="fas fa-external-link-alt"></i></sup></a></td>
            <td>${formatSeconds(row.moving_time)}</td>
            <td class="text-right">${formatKilometers(row.distance)}</td>
            <td class="text-right">${formatMeters(row.elevation_gain)}</td>
            <td class="text-center">${row.private ? '<span class="badge badge-secondary">Private</span>' : ''}</td>
            <td class="text-center">${row.commute ? '<span class="badge badge-secondary">Commute</span>' : ''}</td>
            <td>${row.device_name ? cutText(row.device_name, 25) : '<span class="text-muted">&ndash;</span>'}</td>
            <td>${row.gear_name ? cutText(row.gear_name, 25) : '<span class="text-muted">&ndash;</span>'}</td>
        `
    }
}

export { ActivityTable }