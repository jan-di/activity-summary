import { formatKilometers, formatSeconds } from "../util/Formatter";
import { Chart } from "./Chart";

class AnnualCumulativeChart extends Chart {
    fetchUrl() {
        return '/api/chart/annual-cumulative';
    }

    getChartOptions(data, global) {
        let tooltipFormatter;
        let labelFormatter;
        let labelTitle;
        switch (global.value) {
            case 'distance':
                tooltipFormatter = function() {return `<b>${this.point.custom.start}</b>
                    <br/>${formatKilometers(this.y)} km (${formatKilometers(this.point.custom.value)} km)`}
                labelFormatter = function() {return formatKilometers(this.value)}
                labelTitle = 'Distance (km)'
                break;
            case 'movingTime':
                tooltipFormatter = function() {return `<b>${this.point.custom.start}</b>
                    <br/>${formatSeconds(this.y)} (${formatSeconds(this.point.custom.value)})`}
                labelFormatter = function() {return formatSeconds(this.value)}
                labelTitle = 'Moving Time (HH:MM:SS)'
                break;
        }

        let that = this;

        let fixLabels = function () {
            if (typeof this.xAxis[0].tickPositions !== 'undefined') {
                var labels = $('div.highcharts-xaxis-labels span', this.container).sort(function (a, b) {
                    return +parseInt($(a).css('left')) - +parseInt($(b).css('left'));
                });
                labels.css('margin-left', (parseInt($(labels.get(1)).css('left')) - parseInt($(labels.get(0)).css('left'))) / 2);
                $(labels.get(this.xAxis[0].tickPositions.length - 1)).remove();
            }
        };

        return {
            chart: {
                type: 'line',
                events: {
                    load: fixLabels,
                    redraw: fixLabels
                }
            },

            xAxis: {
                type: 'datetime',
                title: {
                    text: null
                },
                gridLineWidth: 1,
                dateTimeLabelFormats: {
                    month: '%b',
                    year: '%b'
                },
                tickInterval: 30 * 24 * 3600 * 1000,
                min: Date.UTC(1972, 0, 0),
                max: Date.UTC(1972, 11, 32),
                labels: {
                    useHTML: true,
                    align: 'center',
                    formatter: function () {
                        return that.highcharts.dateFormat(this.dateTimeLabelFormat, this.value);
                    }
                }
    
            },

            yAxis: {
                labels: {
                    formatter: labelFormatter
                },
                title: {
                    text: labelTitle
                }
            },

            tooltip: {
                formatter: tooltipFormatter
            },

            series: data.series,
        
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }
        
        }
    }
}

export { AnnualCumulativeChart }