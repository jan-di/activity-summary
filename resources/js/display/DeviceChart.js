import { Chart } from "./Chart";

class DeviceChart extends Chart {
    fetchUrl() {
        return '/api/chart/devices';
    }

    getChartOptions(data) {
        return {
            chart: {
                type: 'pie',
                height: 300
            },

            series: [
                {
                    name: 'Activities',
                    data: data.series[0].data,
                }
            ],
        }
    }
}

export { DeviceChart }