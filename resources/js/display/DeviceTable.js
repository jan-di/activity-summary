import { Table } from "./Table";
import { cutText, formatKilometers, formatSeconds } from "../util/Formatter";

class DeviceTable extends Table {
    fetchUrl() {
        return '/api/table/devices';
    }
    
    renderHeader() {
        return `
            <th scope="col" class="text-center">#</th>
            <th scope="col">Name</th>
            <th class="text-right" scope="col">Activities</th>
            <th class="text-right" scope="col">Total Distance <sup>km</sup></th>
            <th class="text-right" scope="col">Total Time</th>
        `;
    }

    renderBody(row, index) {
        return `
            <th scope="row" class="text-center">${index}</th>
            <td>${row.name ? cutText(row.name, 30) : '<span class="text-muted">&ndash;</span>'}</td>
            <td class="text-right">${row.activity_count}</td>
            <td class="text-right">${formatKilometers(row.total_distance)}</td>
            <td class="text-right">${formatSeconds(row.total_time)}</td>  
        `;
    }
}

export { DeviceTable }