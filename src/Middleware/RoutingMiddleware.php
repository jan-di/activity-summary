<?php

declare(strict_types=1);

namespace App\Middleware;

use App\Service\Response;
use App\Service\Routing;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\NoConfigurationException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class RoutingMiddleware implements MiddlewareInterface
{
    public function __construct(private Routing $routing, private Response $responseFactory)
    {
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            $routeInfo = $this->routing->matchRequest($request);
        } catch (ResourceNotFoundException) {
            $error = [404, 'Not Found'];
        } catch (MethodNotAllowedException) {
            $error = [405, 'Method Not Allowed'];
        } catch (NoConfigurationException) {
            $error = [500, 'Internal Server Error'];
        }

        if (isset($error)) {
            $type = $this->responseFactory->getBestResponseType($request);

            return $this->responseFactory->createErrorResponse($type, $error[1], $error[0]);
        }

        foreach ($routeInfo ?? [] as $key => $value) {
            $request = $request->withAttribute($key, $value);
        }

        return $handler->handle($request);
    }
}
