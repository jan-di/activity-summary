<?php

declare(strict_types=1);

namespace App\Middleware;

use App\Service\Response;
use App\Service\Routing;
use App\Service\Session;
use App\Service\StravaAuth;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class SecurityMiddleware implements MiddlewareInterface
{
    public function __construct(
        private Session $session,
        private Response $responseFactory
    ) {
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $routeRequireAuthentication = $request->getAttribute(Routing::REQUIRE_AUTHENTICATION, false);

        if (!$this->session->isAuthenticated() && $routeRequireAuthentication) {
            $type = $this->responseFactory->getBestResponseType($request);

            return match ($type) {
                Response::TYPE_HTML => $this->responseFactory->createRedirectResponse('security.login', [
                    'event' => StravaAuth::EVENT_REQUIRE_AUTHENTICATION,
                ]),
                default => $this->responseFactory->createErrorResponse($type, 'Authentication Required', 401),
            };
        }

        return $handler->handle($request);
    }
}
