<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\MessageQueue;
use ReflectionClass;
use ReflectionNamedType;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use UnhandledMatchError;

class DebugAddTaskCommand extends Command
{
    protected static $defaultName = 'debug:add-task';

    public function __construct(
        private MessageQueue $messageQueue
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('class_name', InputArgument::REQUIRED, 'FQDN of Task')
            ->addArgument('parameters', InputArgument::OPTIONAL | InputArgument::IS_ARRAY, 'Parameters')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Adding task to queue..');

        /**
         * @var string $className
         * @psalm-var class-string $className
         */
        $className = $input->getArgument('class_name');
        /** @var string[] $parameters */
        $parameters = $input->getArgument('parameters');

        $reflector = new ReflectionClass($className);
        $methodParameters = $reflector->getMethod('create')->getParameters();
        if (count($methodParameters) !== count($parameters)) {
            $io->error(sprintf('Given parameter count is not matching task signature (Given %u, Method %u)', count($parameters), count($methodParameters)));

            return Command::FAILURE;
        }

        for ($i = 0; $i < count($methodParameters); ++$i) {
            $methodParameter = $methodParameters[$i];
            /** @var ReflectionNamedType $reflectionType */
            $reflectionType = $methodParameter->getType();
            $type = $reflectionType->getName();
            try {
                $parameters[$i] = match ($type) {
                'string' => $parameters[$i],
                'int' => intval($parameters[$i])
            };
            } catch (UnhandledMatchError) {
                $io->error(sprintf('Unsupported type %s at parameter %s', $type ?? 'null', $methodParameter->getName()));
            }
        }

        $io->note(sprintf('Add: %s %s', $className::getTaskName(), json_encode($parameters)));

        $task = $className::create(...$parameters);
        $this->messageQueue->publish(json_encode($task), 0);

        return Command::SUCCESS;
    }
}
