<?php

declare(strict_types=1);

namespace App\Command\Strava;

use App\Entity\Athlete;
use App\Repository\AthleteRepository;
use App\Service\StravaAuth;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class RefreshTokensCommand extends Command
{
    protected static $defaultName = 'strava:refresh-tokens';

    public function __construct(
        private EntityManagerInterface $entityManager,
        private StravaAuth $stravaAuth
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Refreshing tokens..');

        /** @var AthleteRepository $athleteRepository */
        $athleteRepository = $this->entityManager->getRepository(Athlete::class);
        $athletes = $athleteRepository->findAthletesForTokenRefresh();

        if (count($athletes) > 0) {
            /** @var Athlete $athlete */
            foreach ($athletes as $athlete) {
                $refreshToken = $athlete->getRefreshToken();
                if ($refreshToken !== null) {
                    $response = $this->stravaAuth->refreshToken($refreshToken);

                    $athlete
                        ->setAccessToken($response->access_token)
                        ->setRefreshToken($response->refresh_token)
                        ->setTokenExpiresAt((new DateTime())->setTimestamp($response->expires_at));

                    $this->entityManager->persist($athlete);

                    $io->writeln('Athlete: '.$athlete->getId());
                }
            }

            $this->entityManager->flush();
            $io->success('Access tokens refreshed successfully');
        } else {
            $io->note('No token due for refreshing.');
        }

        return Command::SUCCESS;
    }
}
