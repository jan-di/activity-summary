<?php

declare(strict_types=1);

namespace App\Command\Strava;

use App\Service\Routing;
use App\Service\StravaHook;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateWebhookCommand extends Command
{
    protected static $defaultName = 'strava:webhook:create';

    public function __construct(
        private StravaHook $stravaHook,
        private Routing $routing,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('hook_url', InputArgument::OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Creating webhook..');

        /** @var string $hookUrl */
        $hookUrl = $input->getArgument('hook_url') ?? $this->routing->generateAbsoluteUrl('webhook.event', [
            'token' => $this->stravaHook->getWebhookToken(),
        ]);
        $io->writeln('Webhook URL: '.$hookUrl);

        $response = $this->stravaHook->createSubscription($hookUrl);

        $io->definitionList(['Subscription ID' => $response->id]);
        $io->success('Successfully created webhook');

        return Command::SUCCESS;
    }
}
