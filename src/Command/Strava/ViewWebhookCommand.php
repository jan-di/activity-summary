<?php

declare(strict_types=1);

namespace App\Command\Strava;

use App\Service\StravaHook;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ViewWebhookCommand extends Command
{
    protected static $defaultName = 'strava:webhook:view';

    public function __construct(
        private StravaHook $stravaHook
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Fetching webhook info..');

        $webhooks = $this->stravaHook->viewSubscription();
        if (count($webhooks) > 0) {
            foreach ($webhooks as $webhook) {
                $io->definitionList(
                    ['Subscription ID' => $webhook->id],
                    ['Application ID' => $webhook->application_id],
                    ['Callback Url' => $webhook->callback_url],
                    ['Created' => $webhook->created_at],
                    ['Updated' => $webhook->updated_at]
                );
            }
        } else {
            $io->note('No active webhooks');
        }

        return Command::SUCCESS;
    }
}
