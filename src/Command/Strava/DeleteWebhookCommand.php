<?php

declare(strict_types=1);

namespace App\Command\Strava;

use App\Service\StravaHook;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DeleteWebhookCommand extends Command
{
    protected static $defaultName = 'strava:webhook:delete';

    public function __construct(
        private StravaHook $stravaHook
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('subscription_id', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Deleting webhook..');

        $subscriptionId = intval($input->getArgument('subscription_id'));
        $io->writeln('Subscription ID: '.$subscriptionId);

        $this->stravaHook->deleteSubscription($subscriptionId);

        $io->success('Successfully deleted webhook');

        return Command::SUCCESS;
    }
}
