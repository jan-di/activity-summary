<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\Routing;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\Router;

class DebugRoutesCommand extends Command
{
    protected static $defaultName = 'debug:routes';

    public function __construct(private Router $router)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $routes = $this->router->getRouteCollection()->all();

        $routeInfo = array_map(function (string $name, Route $route) {
            $requestHandler = gettype($route->getDefault('request-handler') == 'array') ? implode('::', $route->getDefault('request-handler')) : $route->getDefault('request-handler');
            if (substr($requestHandler, 0, 14) == 'App\Controller') {
                $requestHandler = substr($requestHandler, 15);
            }
            $methods = count($route->getMethods()) > 0 ? implode(',', $route->getMethods()) : 'ANY';
            $requireAuthentication = ($route->getDefault(Routing::REQUIRE_AUTHENTICATION) ?? false) ? 'Yes' : 'No';

            return [
                $name,
                $methods,
                $route->getPath(),
                $requestHandler,
                $requireAuthentication,
            ];
        }, array_keys($routes), $routes);

        $io->table(['Name', 'Methods', 'Path', 'Request Handler', 'Req. Auth'], $routeInfo);

        return Command::SUCCESS;
    }
}
