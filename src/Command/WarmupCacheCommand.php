<?php

declare(strict_types=1);

namespace App\Command;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Routing\Router;
use Twig\Environment;
use Twig\Error\SyntaxError;

class WarmupCacheCommand extends Command
{
    protected static $defaultName = 'cache:warmup';

    public function __construct(
        private string $appEnv,
        private Environment $twig,
        private string $twigTemplateDir,
        private Router $router,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var Application $application */
        $application = $this->getApplication();
        $io = new SymfonyStyle($input, $output);

        $io->title('Warming up cache..');

        if ($this->appEnv === 'production') {
            // Config and Container are automaically warmed up by ConsoleApplication at this point

            $io->writeln('Compile annotated Routes..');
            $this->router->match('/');
            $io->newLine();

            $io->writeln('Compile Twig Templates..');
            $io->newLine();
            $templateIterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->twigTemplateDir, RecursiveDirectoryIterator::SKIP_DOTS));
            foreach ($templateIterator as $file) {
                if ($templateIterator->getExtension() === 'twig') {
                    try {
                        $this->twig->render($templateIterator->getSubPathName());
                        $io->writeln(' Processing template: "<fg=green>'.$templateIterator->getSubPathName().'</>"');
                    } catch (SyntaxError) {
                    }
                }
            }
            $io->newLine();

            $io->writeln('Generate Doctrine Proxies..');
            $generateProxiesCommand = $application->get('orm:generate-proxies');
            $generateProxiesCommand->run(new ArrayInput([]), $output);
            $io->newLine();

            $io->success('Cache warmed up successfully');
        } else {
            $io->note('Skipped cache-warmup (Server does not run in production environment)');
        }

        return Command::SUCCESS;
    }
}
