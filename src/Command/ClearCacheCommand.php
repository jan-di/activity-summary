<?php

declare(strict_types=1);

namespace App\Command;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ClearCacheCommand extends Command
{
    protected static $defaultName = 'cache:clear';

    public function __construct(private string $globalCacheDir)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Clearing cache..');

        if (is_dir($this->globalCacheDir)) {
            $cacheFiles = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($this->globalCacheDir, RecursiveDirectoryIterator::SKIP_DOTS),
                RecursiveIteratorIterator::CHILD_FIRST
            );
            foreach ($cacheFiles as $fileinfo) {
                if ($fileinfo->isDir()) {
                    rmdir($fileinfo->getRealPath());
                } else {
                    unlink($fileinfo->getRealPath());
                }
            }
        }

        $io->success('Cache cleared successfully');

        return Command::SUCCESS;
    }
}
