<?php

declare(strict_types=1);

namespace App\Task;

use App\Entity\Activity;
use Doctrine\ORM\EntityManagerInterface;

class DeleteActivityTask extends AbstractTask
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    public static function getTaskName(): string
    {
        return 'delete_activity';
    }

    public static function getTaskPriority(): int
    {
        return 0;
    }

    public static function create(int $activityId): array
    {
        return self::createMessage(self::getTaskName(), [$activityId], self::getTaskPriority());
    }

    public function execute(int $activityId): void
    {
        $this->prepareEntityManager($this->entityManager);

        // delete activity
        $activityRepository = $this->entityManager->getRepository(Activity::class);
        /** @var Activity|null $activity */
        $activity = $activityRepository->find($activityId);
        if ($activity !== null) {
            $this->entityManager->remove($activity);
            $this->entityManager->flush();
        }
    }
}
