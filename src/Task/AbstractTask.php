<?php

declare(strict_types=1);

namespace App\Task;

use Doctrine\ORM\EntityManagerInterface;
use LogicException;

abstract class AbstractTask
{
    abstract public static function getTaskName(): string;

    abstract public static function getTaskPriority(): int;

    protected static function createMessage(string $taskName, array $parameters, int $priority): array
    {
        if ($taskName === '') {
            throw new LogicException('Task Name may not be empty.');
        }

        return [
            'name' => $taskName,
            'parameters' => $parameters,
            'priority' => $priority,
        ];
    }

    protected function prepareEntityManager(EntityManagerInterface $entityManager): void
    {
        // detach all cached entities to prevent memory overflows
        $entityManager->clear();

        // ensure that the database is connected
        $connection = $entityManager->getConnection();
        $connection->close();
        $connection->connect();
    }
}
