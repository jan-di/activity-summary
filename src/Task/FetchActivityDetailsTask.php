<?php

declare(strict_types=1);

namespace App\Task;

use App\Entity\Activity;
use App\Entity\Athlete;
use App\Entity\Gear;
use App\Service\StravaApi;
use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;

class FetchActivityDetailsTask extends AbstractTask
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private StravaApi $stravaApi
    ) {
    }

    public static function getTaskName(): string
    {
        return 'fetch_activity_details';
    }

    public static function getTaskPriority(): int
    {
        return 1;
    }

    public static function create(int $athleteId, int $activityId): array
    {
        return self::createMessage(self::getTaskName(), [$athleteId, $activityId], self::getTaskPriority());
    }

    public function execute(int $athleteId, int $activityId): void
    {
        $this->prepareEntityManager($this->entityManager);

        // get athlete
        $athleteRepository = $this->entityManager->getRepository(Athlete::class);
        /** @var Athlete|null $athlete */
        $athlete = $athleteRepository->find($athleteId);
        if ($athlete === null) {
            throw new RuntimeException('Athlete not found');
        }

        // api request
        $this->stravaApi->setAccessToken($athlete->getAccessToken());
        $activityResource = $this->stravaApi->getActivityById($activityId);

        // get gear (optional)
        $gearResource = $activityResource->gear ?? null;
        if ($gearResource === null) {
            $gear = null;
        } else {
            $gearRepository = $this->entityManager->getRepository(Gear::class);
            /** @var Gear|null $gear */
            $gear = $gearRepository->find($activityResource->gear?->id);
            if ($gear === null) {
                $gear = new Gear($gearResource, $athlete);
                $this->entityManager->persist($gear);
            } else {
                $gear->update($gearResource);
            }
        }

        // create/update activity
        $activityRepository = $this->entityManager->getRepository(Activity::class);
        /** @var Activity|null $activity */
        $activity = $activityRepository->find($activityId);
        if ($activity === null) {
            $activity = new Activity($activityResource, $athlete, $gear);
            $this->entityManager->persist($activity);
        } else {
            $activity->update($activityResource, $gear);
        }

        $this->entityManager->flush();
    }
}
