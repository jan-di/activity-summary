<?php

declare(strict_types=1);

namespace App\Task;

use App\Entity\Activity;
use App\Entity\Athlete;
use App\Entity\Gear;
use App\Repository\ActivityRepository;
use App\Repository\GearRepository;
use Doctrine\ORM\EntityManagerInterface;

class DeleteAthleteTask extends AbstractTask
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    public static function getTaskName(): string
    {
        return 'delete_athlete';
    }

    public static function getTaskPriority(): int
    {
        return 0;
    }

    public static function create(int $athleteId): array
    {
        return self::createMessage(self::getTaskName(), [$athleteId], self::getTaskPriority());
    }

    public function execute(int $athleteId): void
    {
        $this->prepareEntityManager($this->entityManager);

        // delete activities
        /** @var ActivityRepository $activityRepository */
        $activityRepository = $this->entityManager->getRepository(Activity::class);
        $activityRepository->deleteByAthleteId($athleteId);

        // delete gears
        /** @var GearRepository $gearRepository */
        $gearRepository = $this->entityManager->getRepository(Gear::class);
        $gearRepository->deleteByAthleteId($athleteId);

        // delete athlete
        $athleteRepository = $this->entityManager->getRepository(Athlete::class);
        /** @var Athlete|null $athlete */
        $athlete = $athleteRepository->find($athleteId);
        if ($athlete !== null) {
            $this->entityManager->remove($athlete);
            $this->entityManager->flush();
        }
    }
}
