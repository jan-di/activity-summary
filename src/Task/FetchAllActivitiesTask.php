<?php

declare(strict_types=1);

namespace App\Task;

use App\Entity\Activity;
use App\Entity\Athlete;
use App\Service\MessageQueue;
use App\Service\StravaApi;
use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;

class FetchAllActivitiesTask extends AbstractTask
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private StravaApi $stravaApi,
        private MessageQueue $messageQueue,
    ) {
    }

    public static function getTaskName(): string
    {
        return 'fetch_all_activities';
    }

    public static function getTaskPriority(): int
    {
        return 2;
    }

    public static function create(int $athleteId): array
    {
        return self::createMessage(self::getTaskName(), [$athleteId], self::getTaskPriority());
    }

    public function execute(int $athleteId): void
    {
        $this->prepareEntityManager($this->entityManager);

        // get athlete
        $athleteRepository = $this->entityManager->getRepository(Athlete::class);
        /** @var Athlete|null $athlete */
        $athlete = $athleteRepository->find($athleteId);
        if ($athlete === null) {
            throw new RuntimeException('Athlete not found');
        }

        // prepare
        $this->stravaApi->setAccessToken($athlete->getAccessToken());
        $activityRepository = $this->entityManager->getRepository(Activity::class);

        $page = 1;
        do {
            $activityResources = $this->stravaApi->getLoggedInAthleteActivities($page, StravaApi::PAGINATION_MAX);
            if (count($activityResources) > 0) {
                foreach ($activityResources as $activityResource) {
                    // create/update activity based on summary
                    /** @var Activity|null $activity */
                    $activity = $activityRepository->find($activityResource->id);
                    if ($activity === null) {
                        $activity = new Activity($activityResource, $athlete, null);
                        $this->entityManager->persist($activity);
                    } else {
                        $activity->update($activityResource, null);
                    }

                    // fetch detailed representation
                    if (!$activity->isDetailed()) {
                        $task = FetchActivityDetailsTask::create($athlete->getId(), $activity->getId());
                        $this->messageQueue->publish(json_encode($task), FetchActivityDetailsTask::getTaskPriority());
                    }
                }
                $this->entityManager->flush();
                ++$page;
            }
        } while (count($activityResources) > 0);
    }
}
