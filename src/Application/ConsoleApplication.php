<?php

declare(strict_types=1);

namespace App\Application;

use App\Application\Bootstrap\ConfigLoader;
use App\Application\Bootstrap\ContainerFactory;
use App\Command\ClearCacheCommand;
use App\Command\DebugAddTaskCommand;
use App\Command\DebugRoutesCommand;
use App\Command\Strava\CreateWebhookCommand;
use App\Command\Strava\DeleteWebhookCommand;
use App\Command\Strava\RefreshTokensCommand;
use App\Command\Strava\ViewWebhookCommand;
use App\Command\WarmupCacheCommand;
use Doctrine\Migrations\Tools\Console\Command\CurrentCommand as DoctrineMigrationsCurrentCommand;
use Doctrine\Migrations\Tools\Console\Command\DiffCommand as DoctrineMigrationsDiffCommand;
use Doctrine\Migrations\Tools\Console\Command\DumpSchemaCommand as DoctrineMigrationsDumpSchemaCommand;
use Doctrine\Migrations\Tools\Console\Command\ExecuteCommand as DoctrineMigrationsExecuteCommand;
use Doctrine\Migrations\Tools\Console\Command\GenerateCommand as DoctrineMigrationsGenerateCommand;
use Doctrine\Migrations\Tools\Console\Command\LatestCommand as DoctrineMigrationsLatestCommand;
use Doctrine\Migrations\Tools\Console\Command\ListCommand as DoctrineMigrationsListCommand;
use Doctrine\Migrations\Tools\Console\Command\MigrateCommand as DoctrineMigrationsMigrateCommand;
use Doctrine\Migrations\Tools\Console\Command\RollupCommand as DoctrineMigrationsRollupCommand;
use Doctrine\Migrations\Tools\Console\Command\StatusCommand as DoctrineMigrationsStatusCommand;
use Doctrine\Migrations\Tools\Console\Command\SyncMetadataCommand  as DoctrineMigrationsSyncMetadataCommand;
use Doctrine\Migrations\Tools\Console\Command\UpToDateCommand as DoctrineMigrationsUpToDateCommand;
use Doctrine\Migrations\Tools\Console\Command\VersionCommand as DoctrineMigrationsVersionCommand;
use Doctrine\ORM\Tools\Console\Command\GenerateProxiesCommand as DoctrineOrmGenerateProxiesCommand;
use Doctrine\ORM\Tools\Console\Command\SchemaTool\CreateCommand as DoctrineOrmSchemaToolCreateCommand;
use Doctrine\ORM\Tools\Console\Command\SchemaTool\DropCommand as DoctrineOrmSchemaToolDropCommand;
use Doctrine\ORM\Tools\Console\Command\SchemaTool\UpdateCommand as DoctrineOrmSchemaToolUpdateCommand;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use Jandi\Config\Exception\BuildException;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Helper\HelperSet;

final class ConsoleApplication
{
    private Application $application;

    public function __construct()
    {
        $configLoader = new ConfigLoader();
        try {
            $config = $configLoader->loadConfig();
        } catch (BuildException $e) {
            echo $e->getTextSummary();
            exit(1);
        }
        $containerFactory = new ContainerFactory();
        $container = $containerFactory->createContainer($config);

        $this->application = new Application($container->get('app.title'));
        $this->application->setCatchExceptions(true);
        $this->application->setHelperSet(new HelperSet([
            'em' => $container->get(EntityManagerHelper::class),
        ]));

        $commands = [
            ClearCacheCommand::class,
            WarmupCacheCommand::class,
            RefreshTokensCommand::class,
            CreateWebhookCommand::class,
            ViewWebhookCommand::class,
            DeleteWebhookCommand::class,
            DoctrineOrmGenerateProxiesCommand::class,
            DoctrineOrmSchemaToolCreateCommand::class,
            DoctrineOrmSchemaToolUpdateCommand::class,
            DoctrineOrmSchemaToolDropCommand::class,
            DoctrineMigrationsStatusCommand::class,
            DoctrineMigrationsMigrateCommand::class,
            DoctrineMigrationsListCommand::class,
            DoctrineMigrationsExecuteCommand::class,
            DoctrineMigrationsCurrentCommand::class,
            DoctrineMigrationsLatestCommand::class,
            DoctrineMigrationsUpToDateCommand::class,
            DoctrineMigrationsVersionCommand::class,
            DoctrineMigrationsSyncMetadataCommand::class,
        ];
        if ($container->get('app.env') === 'development') {
            $commands = array_merge($commands, [
                DebugRoutesCommand::class,
                DebugAddTaskCommand::class,
                DoctrineMigrationsGenerateCommand::class,
                DoctrineMigrationsDiffCommand::class,
                DoctrineMigrationsDumpSchemaCommand::class,
                DoctrineMigrationsRollupCommand::class,
            ]);
        }

        foreach ($commands as $command) {
            $this->application->add($container->get($command));
        }
    }

    public function execute(): int
    {
        return $this->application->run();
    }
}
