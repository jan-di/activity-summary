<?php

declare(strict_types=1);

namespace App\Application\Bootstrap;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\WebProcessor;
use Psr\Log\LoggerInterface;

class LoggerFactory
{
    public function createErrorHandlerLogger(string $fileName): LoggerInterface
    {
        $log = new Logger('error-handler');
        $log->pushHandler(new StreamHandler($fileName, Logger::CRITICAL));
        $log->pushProcessor(new WebProcessor());

        return $log;
    }

    public function createWebhookLogger(string $fileName): LoggerInterface
    {
        $log = new Logger('webhook');
        $log->pushHandler(new StreamHandler($fileName, Logger::INFO));

        return $log;
    }

    public function createStravaApiLogger(string $fileName): LoggerInterface
    {
        $log = new Logger('strava-api');
        $log->pushHandler(new StreamHandler($fileName, Logger::INFO));

        return $log;
    }

    public function createWorkerLogger(string $fileName): LoggerInterface
    {
        $log = new Logger('worker');
        $log->pushHandler(new StreamHandler($fileName, Logger::INFO));

        return $log;
    }
}
