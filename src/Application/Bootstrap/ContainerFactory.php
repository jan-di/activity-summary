<?php

declare(strict_types=1);

namespace App\Application\Bootstrap;

use DI\ContainerBuilder;
use Jandi\Config\Config;
use Psr\Container\ContainerInterface;

class ContainerFactory
{
    public function createContainer(Config $config): ContainerInterface
    {
        $builder = new ContainerBuilder();
        $builder->useAutowiring(true);
        $builder->useAnnotations(false);
        $builder->addDefinitions($config->exportValues('config:'));
        $builder->addDefinitions(...glob(__DIR__.'/../../../config/container/*.php'));

        if ($config->getValue('APP_ENV') === 'production') {
            $builder->enableCompilation(__DIR__.'/../../../var/cache/container');
        }

        return $builder->build();
    }
}
