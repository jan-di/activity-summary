<?php

declare(strict_types=1);

namespace App\Application\Bootstrap;

use Doctrine\Common\Annotations\AnnotationReader;
use ReflectionClass;
use ReflectionMethod;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\AnnotationClassLoader;
use Symfony\Component\Routing\Loader\AnnotationDirectoryLoader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\Router;

class RouterFactory
{
    public function createRouter(
        string $appEnv,
        string $cacheDir,
        string $controllerDir
    ): Router {
        $options = [];

        if ($appEnv === 'production') {
            $options['cache_dir'] = $cacheDir;
        }

        $annotationReader = new AnnotationReader();
        $annotatedRouteLoader = new class($annotationReader) extends AnnotationClassLoader {
            protected function configureRoute(Route $route, ReflectionClass $class, ReflectionMethod $method, object $annot): void
            {
                if ($method->getName() === '__invoke') {
                    $route->setDefault('request-handler', $class->getName());
                } else {
                    $route->setDefault('request-handler', [$class->getName(), $method->getName()]);
                }
            }
        };

        $router = new Router(
            new AnnotationDirectoryLoader(
                new FileLocator(),
                $annotatedRouteLoader
            ),
            $controllerDir,
            $options
        );

        return $router;
    }
}
