<?php

declare(strict_types=1);

namespace App\Application\Bootstrap;

use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\Migrations\Configuration\EntityManager\ExistingEntityManager;
use Doctrine\Migrations\Configuration\Migration\ConfigurationArray;
use Doctrine\Migrations\DependencyFactory;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Setup;

class DoctrineFactory
{
    public function createEntityManager(
        string $appEnv,
        string $entityDir,
        string $entityNamespace,
        string $proxyDir,
        string $cacheDir,
        array $connection
    ): EntityManagerInterface {
        $isDevMode = $appEnv === 'development';

        $configuration = Setup::createAnnotationMetadataConfiguration(
            [$entityDir],
            $isDevMode,
            $proxyDir,
            $isDevMode ? null : new FilesystemCache($cacheDir),
            false
        );
        $configuration->addEntityNamespace('E', $entityNamespace);

        return EntityManager::create($connection, $configuration);
    }

    public function createDependencyFactory(
        EntityManagerInterface $entityManager,
        string $migrationsDir,
        string $migrationsNamespace
    ): DependencyFactory {
        $configurationLoader = new ConfigurationArray([
            'table_storage' => [
                'table_name' => 'database_versions',
            ],
            'migrations_paths' => [
                $migrationsNamespace => $migrationsDir,
            ],
        ]);

        return DependencyFactory::fromEntityManager(
            $configurationLoader,
            new ExistingEntityManager($entityManager)
        );
    }
}
