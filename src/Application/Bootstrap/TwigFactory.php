<?php

declare(strict_types=1);

namespace App\Application\Bootstrap;

use App\Service\Routing;
use Psr\Container\ContainerInterface;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;

class TwigFactory
{
    public function createEnvironment(
        string $appEnv,
        string $cacheDir,
        string $templateDir,
        ContainerInterface $container,
        Routing $routing
    ): Environment {
        $options = [];

        if ($appEnv === 'production') {
            $options['cache'] = $cacheDir;
        }

        $loader = new FilesystemLoader($templateDir);

        $environment = new Environment($loader, $options);
        $environment->addFunction(new TwigFunction('get', [$container, 'get']));
        $environment->addFunction(new TwigFunction('link', [$routing, 'generateUrl']));
        $environment->addFunction(new TwigFunction('absoluteLink', [$routing, 'generateAbsoluteUrl']));

        return $environment;
    }
}
