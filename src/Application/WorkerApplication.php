<?php

declare(strict_types=1);

namespace App\Application;

use App\Application\Bootstrap\ConfigLoader;
use App\Application\Bootstrap\ContainerFactory;
use App\Service\Exception\TaskWorkerException;
use App\Service\MessageQueue;
use App\Service\TaskWorker;
use App\Task\DeleteActivityTask;
use App\Task\DeleteAthleteTask;
use App\Task\FetchActivityDetailsTask;
use App\Task\FetchAllActivitiesTask;
use Jandi\Config\Exception\BuildException;
use PhpAmqpLib\Message\AMQPMessage;

final class WorkerApplication
{
    private MessageQueue $messageQueue;
    private TaskWorker $taskWorker;

    public function __construct()
    {
        $configLoader = new ConfigLoader();
        try {
            $config = $configLoader->loadConfig();
        } catch (BuildException $e) {
            echo $e->getTextSummary();
            exit(1);
        }
        $containerFactory = new ContainerFactory();
        $container = $containerFactory->createContainer($config);

        $logger = $container->get('log.worker');
        $this->taskWorker = new TaskWorker([
            FetchActivityDetailsTask::class,
            DeleteActivityTask::class,
            FetchAllActivitiesTask::class,
            DeleteAthleteTask::class,
        ], $container, $logger);

        $this->messageQueue = $container->get(MessageQueue::class);
        $this->messageQueue->setQoS();
    }

    public function execute(): int
    {
        echo "Start task worker..\n";
        $taskWorker = $this->taskWorker;

        $consumerTag = $this->messageQueue->consume(function (AMQPMessage $message) use ($taskWorker) {
            $task = json_decode($message->getBody());

            echo 'P'.$task->priority.' '.$task->name.' '.json_encode($task->parameters);
            try {
                $taskWorker->process($task);
                echo " done\n";
                $message->ack();
            } catch (TaskWorkerException $e) {
                echo " failed\n";
                $message->nack();
            }
        });

        echo 'consumer tag: '.$consumerTag."\n";
        $this->messageQueue->waitForConsuming();
        $this->messageQueue->close();

        return 0;
    }
}
