<?php

declare(strict_types=1);

namespace App\Application;

use App\Application\Bootstrap\ConfigLoader;
use App\Application\Bootstrap\ContainerFactory;
use App\Middleware\ErrorHandlerMiddleware;
use App\Middleware\RoutingMiddleware;
use App\Middleware\SecurityMiddleware;
use App\Middleware\SessionMiddleware;
use Jandi\Config\Exception\BuildException;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\HttpHandlerRunner\Emitter\EmitterInterface;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Middleland\Dispatcher;
use Middlewares\RequestHandler;
use Middlewares\Whoops;

final class WebApplication
{
    private Dispatcher $dispatcher;
    private EmitterInterface $emitter;

    public function __construct()
    {
        $configLoader = new ConfigLoader();
        try {
            $config = $configLoader->loadConfig();
        } catch (BuildException $e) {
            echo $e->getHtmlSummary();
            exit(1);
        }
        $containerFactory = new ContainerFactory();
        $container = $containerFactory->createContainer($config);

        $this->dispatcher = new Dispatcher([
            [$container->get('app.env') === 'development', Whoops::class],
            [$container->get('app.env') === 'production', ErrorHandlerMiddleware::class],
            SessionMiddleware::class,
            RoutingMiddleware::class,
            SecurityMiddleware::class,
            RequestHandler::class,
        ], $container);

        $this->emitter = new SapiEmitter();
    }

    public function execute(): void
    {
        $request = ServerRequestFactory::fromGlobals($_SERVER, $_GET, $_POST, $_COOKIE, $_FILES);
        $response = $this->dispatcher->dispatch($request);
        $this->emitter->emit($response);
    }
}
