<?php

declare(strict_types=1);

namespace App\Service;

use Psr\Http\Message\ServerRequestInterface;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Router;

class Routing
{
    // route attributes
    public const REQUIRE_AUTHENTICATION = 'require_authentication';

    private RequestContext $absoluteContext;

    public function __construct(
        private Router $router,
        private HttpFoundationFactory $symfonyRequestFactory,
        string $selfScheme,
        string $selfHost,
        string $selfBase
    ) {
        $this->absoluteContext = new RequestContext($selfBase, 'GET', $selfHost, $selfScheme);
    }

    public function matchRequest(ServerRequestInterface $request): array
    {
        $symfonyRequest = $this->symfonyRequestFactory->createRequest($request);
        $this->router->getContext()->fromRequest($symfonyRequest);

        return $this->router->matchRequest($symfonyRequest);
    }

    public function generateUrl(string $route, array $parameters = []): string
    {
        return $this->router->generate($route, $parameters);
    }

    public function generateAbsoluteUrl(string $route, array $parameters = []): string
    {
        $currentContext = $this->router->getContext();

        $this->router->setContext($this->absoluteContext);
        $url = $this->router->generate($route, $parameters, Router::ABSOLUTE_URL);
        $this->router->setContext($currentContext);

        return $url;
    }
}
