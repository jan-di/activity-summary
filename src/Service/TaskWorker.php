<?php

declare(strict_types=1);

namespace App\Service;

use App\Service\Exception\TaskWorkerException;
use LogicException;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use stdClass;
use Throwable;

class TaskWorker
{
    private array $taskList;

    public function __construct(
        private array $taskClasses,
        private ContainerInterface $container,
        private LoggerInterface $logger,
    ) {
        $this->taskList = [];

        foreach ($taskClasses as $taskClass) {
            $taskName = $taskClass::getTaskName();

            if ($taskName === '') {
                throw new LogicException('Empty task names are not allowed (Class: '.$taskClass.')');
            }
            if (isset($this->taskList[$taskName])) {
                throw new LogicException('Duplikate task name '.$taskName.' (Classes: '.$taskClass.', '.$this->taskList[$taskName].')');
            }

            $this->taskList[$taskName] = $taskClass;
        }
    }

    public function process(stdClass $task): void
    {
        try {
            $taskClass = $this->taskList[$task->name] ?? null;
            $taskExecutor = $this->container->get($taskClass ?? '');
        } catch (Throwable $e) {
            $message = sprintf('There is no task executor registered for "%s"!', $task->name);
            $this->logger->critical($message, [
                'error' => $e,
            ]);

            throw new TaskWorkerException($message, $e);
        }

        $logText = sprintf('P%d %s %s', $task->priority, $task->name, json_encode($task->parameters));
        try {
            $taskExecutor->execute(...$task->parameters);

            $this->logger->info($logText.' OK');
        } catch (Throwable $e) {
            $this->logger->error($logText.' FAIL', [
                'error' => $e,
            ]);

            throw new TaskWorkerException('Task execution has failed: '.$e->getMessage(), $e);
        }
    }
}
