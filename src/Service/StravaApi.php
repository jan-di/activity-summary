<?php

declare(strict_types=1);

namespace App\Service;

use App\Service\Exception\StravaApiException;
use DateTimeImmutable;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use stdClass;

class StravaApi
{
    public const RESOURCE_STATE_META = 1;
    public const RESOURCE_STATE_SUMMARY = 2;
    public const RESOURCE_STATE_DETAILED = 3;

    public const PAGINATION_MAX = 200;

    private ?string $accessToken = null;
    private ?int $shortTermUsage = null;
    private ?int $shortTermLimit = null;
    private ?DateTimeImmutable $shortTermReset = null;
    private ?int $longTermUsage = null;
    private ?int $longTermLimit = null;
    private ?DateTimeImmutable $longTermReset = null;

    public function __construct(
        private string $apiBaseUrl,
        private Client $client,
        private LoggerInterface $logger,
    ) {
    }

    /**
     * @see https://developers.strava.com/docs/reference/#api-Athletes-getLoggedInAthlete
     */
    public function getLoggedInAthlete(): stdClass
    {
        $response = $this->apiRequest('get', $this->apiBaseUrl.'/athlete');

        return json_decode($response->getBody()->__toString());
    }

    /**
     * @see https://developers.strava.com/docs/reference/#api-Activities-getActivityById
     */
    public function getActivityById(int $activityId): stdClass
    {
        $response = $this->apiRequest('get', $this->apiBaseUrl.'/activities/'.$activityId);

        return json_decode($response->getBody()->__toString());
    }

    /**
     * @see https://developers.strava.com/docs/reference/#api-Activities-getLoggedInAthleteActivities
     */
    public function getLoggedInAthleteActivities(int $page, int $perPage): array
    {
        $response = $this->apiRequest('get', $this->apiBaseUrl.'/athlete/activities', ['query' => [
            'page' => $page,
            'per_page' => $perPage,
        ]]);

        return json_decode($response->getBody()->__toString());
    }

    public function setAccessToken(?string $token): self
    {
        $this->accessToken = $token;

        return $this;
    }

    private function apiRequest(string $method, string $url, array $options = []): ResponseInterface
    {
        if ($this->accessToken === null) {
            throw new RuntimeException('Must set an access token before making api requests!');
        }
        $options[RequestOptions::HEADERS]['Authorization'] = 'Bearer '.$this->accessToken;

        $relativeApiUrl = substr($url, strlen($this->apiBaseUrl));
        $rateLimitReached = false;
        try {
            try {
                $response = $this->client->request($method, $url, $options);
                $statusCode = $response->getStatusCode();
            } catch (ClientException $e) {
                $response = $e->hasResponse() ? $e->getResponse() : null;
                $statusCode = $response ? $response->getStatusCode() : null;

                if ($response !== null && $statusCode === 429) {
                    $rateLimitReached = true;
                } else {
                    throw $e;
                }
            }
        } catch (RequestException $e) {
            throw new StravaApiException(sprintf('API Request %s %s failed: %s', strtoupper($method), $relativeApiUrl, $e->getMessage()), $e);
        }

        $this->checkRateLimit(
            $response->getHeader('X-RateLimit-Usage')[0],
            $response->getHeader('X-RateLimit-Limit')[0],
            $response->getHeader('Date')[0],
        );

        $this->logger->info(sprintf('%s %s %s %u', strtoupper($method), $relativeApiUrl, json_encode($options['query'] ?? []), $statusCode), [
            'rate-limit' => [
                'short-term' => sprintf('%u/%u', $this->getShortTermUsage() ?? '?', $this->getShortTermLimit() ?? '?'),
                'long-term' => sprintf('%u/%u', $this->getLongTermUsage() ?? '?', $this->getLongTermLimit() ?? '?'),
            ],
        ]);

        if ($rateLimitReached) { // wait and try again
            if ($this->getLongTermRemaining() === 0) {
                $sleepUntil = $this->getLongTermReset();
            } else {
                $sleepUntil = $this->getShortTermReset();
            }
            if ($sleepUntil === null) {
                throw new Exception('Invalid time to wait');
            }
            $this->logger->notice(sprintf('Rate limit reached. Waiting until %s', $sleepUntil->format('c')));
            time_sleep_until(intval($sleepUntil->format('U')) + 1);

            $response = $this->apiRequest($method, $url, $options);
        }

        return $response;
    }

    /**
     * @see https://developers.strava.com/docs/rate-limits/
     */
    private function checkRateLimit(string $usageHeader, string $limitHeader, string $dateHeader): void
    {
        $usage = explode(',', $usageHeader);
        $limit = explode(',', $limitHeader);
        $date = DateTimeImmutable::createFromFormat('D, d M Y H:i:s T', $dateHeader);
        if ($date === false) {
            throw new InvalidArgumentException('Invalid date header');
        }

        $this->shortTermUsage = intval($usage[0]);
        $this->shortTermLimit = intval($limit[0]);
        $this->shortTermReset = $this->ceilTimeToMinute($date, 15);
        $this->longTermUsage = intval($usage[1]);
        $this->longTermLimit = intval($limit[1]);
        $this->longTermReset = $this->ceilTimeToMinute($date, 1440);
    }

    private function ceilTimeToMinute(DateTimeImmutable $datetime, int $precision): DateTimeImmutable
    {
        $timestamp = $datetime->getTimestamp();
        $precision *= 60;

        return (new DateTimeImmutable())->setTimestamp((int) ceil($timestamp / $precision) * $precision);
    }

    public function getShortTermUsage(): ?int
    {
        return $this->shortTermUsage;
    }

    public function getShortTermLimit(): ?int
    {
        return $this->shortTermLimit;
    }

    public function getShortTermRemaining(): ?int
    {
        if ($this->shortTermUsage !== null && $this->shortTermLimit !== null) {
            return $this->shortTermLimit - $this->shortTermUsage;
        }

        return null;
    }

    public function getShortTermReset(): ?DateTimeImmutable
    {
        return $this->shortTermReset;
    }

    public function getLongTermUsage(): ?int
    {
        return $this->longTermUsage;
    }

    public function getLongTermLimit(): ?int
    {
        return $this->longTermLimit;
    }

    public function getLongTermRemaining(): ?int
    {
        if ($this->longTermUsage !== null && $this->longTermLimit !== null) {
            return $this->longTermLimit - $this->longTermUsage;
        }

        return null;
    }

    public function getLongTermReset(): ?DateTimeImmutable
    {
        return $this->longTermReset;
    }
}
