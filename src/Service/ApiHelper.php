<?php

declare(strict_types=1);

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ApiHelper
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private Session $session,
        private Response $response,
    ) {
    }

    public function getAthleteId(ServerRequestInterface $request, string $parameter = 'athlete'): int
    {
        $athleteId = intval($request->getQueryParams()[$parameter] ?? '');

        if ($athleteId <= 0) {
            throw new InvalidArgumentException('Athlete ID is invalid', 400);
        }

        return $athleteId;
    }

    public function checkPermissionForAthleteId(int $athleteId): void
    {
        if ($this->session->getAthleteId() !== $athleteId) {
            throw new InvalidArgumentException('No permission for this athlete', 403);
        }
    }

    public function applyActivityAthleteFilter(array &$clauses, array &$parameters, int $athleteId): void
    {
        $clauses[] = '(activity.athlete_id = :athlete_id)';
        $parameters[':athlete_id'] = $athleteId;
    }

    public function getCommonActivityFilters(ServerRequestInterface $request): array
    {
        return [
            'visibility' => $this->validateMatchValues($request, 'visibility', ['public', 'private']),
            'commute' => $this->validateMatchValues($request, 'commute', ['only', 'except']),
            'years' => $this->validateInValues($request, 'years', '/^[0-9]+$/'),
            'types' => $this->validateInValues($request, 'types', '/^[a-zA-Z]+$/'),
        ];
    }

    public function applyCommonActivityFilters(array &$clauses, array &$parameters, array $filters): void
    {
        $this->addMatchClause($clauses, $filters['visibility'], [
            'public' => 'activity.private IS FALSE',
            'private' => 'activity.private IS TRUE',
        ]);
        $this->addMatchClause($clauses, $filters['commute'], [
            'only' => 'activity.commute IS TRUE',
            'except' => 'activity.commute IS FALSE',
        ]);
        $this->addInClause($clauses, $parameters, $filters['years'], 'activity_year', 'YEAR(activity.start_date)');
        $this->addInClause($clauses, $parameters, $filters['types'], 'activity_type', 'activity.type');
    }

    public function validateMatchValues(ServerRequestInterface $request, string $parameter, array $options, bool $allowNull = true): ?string
    {
        $value = $request->getQueryParams()[$parameter] ?? null;

        if ($value !== null && !in_array($value, $options)) {
            throw new InvalidArgumentException(sprintf('Invalid value for parameter \'%s\'!', $parameter), 400);
        } elseif ($value === null && !$allowNull) {
            throw new InvalidArgumentException(sprintf('Missing value for parameter \'%s\'!', $parameter), 400);
        }

        return $value;
    }

    public function addMatchClause(array &$clauses, ?string $value, array $options): void
    {
        if ($value !== null) {
            $clauses[] = '('.$options[$value].')';
        }
    }

    public function validateInValues(ServerRequestInterface $request, string $parameter, string $regex): ?array
    {
        $values = $request->getQueryParams()[$parameter] ?? null;

        if ($values !== null) {
            if (strlen($values) > 0) {
                $values = explode(',', $values);
                foreach ($values as $part) {
                    if (!preg_match($regex, $part)) {
                        throw new InvalidArgumentException(sprintf('Invalid value for parameter \'%s\'!', $parameter), 400);
                    }
                }
            } else {
                $values = [];
            }
        }

        return $values;
    }

    public function addInClause(array &$clauses, array &$parameters, ?array $values, string $parameterPrefix, string $compareExpression): void
    {
        if ($values !== null) {
            if (count($values) > 0) {
                $inList = [];
                for ($i = 0; $i < count($values); ++$i) {
                    $parameterName = sprintf(':%s_%u', $parameterPrefix, $i);
                    $inList[] = $parameterName;
                    $parameters[$parameterName] = $values[$i];
                }
                $clauses[] = sprintf('(%s IN (%s))', $compareExpression, implode(',', $inList));
            } else {
                $clauses[] = '(FALSE)';
            }
        }
    }

    public function buildWhereClause(array $clauses): string
    {
        return count($clauses) > 0 ? '('.implode(' AND ', $clauses).')' : '';
    }

    public function createErrorResponse(ServerRequestInterface $request, InvalidArgumentException $exception): ResponseInterface
    {
        $type = $this->response->getBestResponseType($request);

        return $this->response->createErrorResponse($type, $exception->getMessage(), intval($exception->getCode()));
    }

    // -------------------------------------------------------------------------------------------

    public function getFilterOptions(int $athleteId): array
    {
        $connection = $this->entityManager->getConnection();
        $result = [
            'athlete' => $athleteId,
        ];

        $stmt = $connection->prepare('SELECT DISTINCT activity.type FROM activity WHERE activity.athlete_id = :athleteId;');
        $stmt->execute(['athleteId' => $athleteId]);
        $result['types'] = $stmt->fetchFirstColumn();

        $stmt = $connection->prepare('SELECT DISTINCT YEAR(activity.start_date) FROM activity WHERE activity.athlete_id = :athleteId;');
        $stmt->execute(['athleteId' => $athleteId]);
        $result['years'] = $stmt->fetchFirstColumn();

        return $result;
    }
}
