<?php

declare(strict_types=1);

namespace App\Service;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPLazyConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;

class MessageQueue
{
    private ?AMQPChannel $channel = null;

    public function __construct(
        private AMQPLazyConnection $connection,
        private string $queue
    ) {
    }

    public function connect(): void
    {
        if ($this->channel === null) {
            $this->channel = $this->connection->channel();

            $this->channel->queue_declare(
                queue: $this->queue,
                passive: false,
                durable: true,
                exclusive: false,
                auto_delete: false,
                nowait: false,
                arguments: new AMQPTable([
                    'x-max-priority' => 2,
                ]),
            );
        }
    }

    public function publish(string $message, int $priority): void
    {
        if ($this->channel === null) {
            $this->connect();
        }

        $amqpMessage = new AMQPMessage(
            body: $message,
            properties: [
                'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT,
                'priority' => $priority,
            ]
        );

        $this->channel->basic_publish(
            msg: $amqpMessage,
            routing_key: $this->queue
        );
    }

    public function consume(callable $callback): string
    {
        if ($this->channel === null) {
            $this->connect();
        }

        return $this->channel->basic_consume(
            queue: $this->queue,
            consumer_tag: '',
            no_local: false,
            no_ack: false,
            exclusive: false,
            nowait: false,
            callback: $callback
        );
    }

    public function waitForConsuming(): void
    {
        if ($this->channel === null) {
            $this->connect();
        }

        while ($this->channel->is_consuming()) {
            $this->channel->wait();
        }
    }

    public function setQoS(): void
    {
        if ($this->channel === null) {
            $this->connect();
        }

        $this->channel->basic_qos(
            prefetch_size: 0,
            prefetch_count: 1, // do not fetch multiple jobs at once
            a_global: false
        );
    }

    public function close(): void
    {
        if ($this->channel !== null) {
            $this->channel->close();
            $this->connection->close();
        }
    }
}
