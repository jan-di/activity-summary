<?php

declare(strict_types=1);

namespace App\Service;

use GuzzleHttp\Client;
use stdClass;

class StravaAuth
{
    public const REQUIRED_SCOPES = ['read', 'activity:read'];
    public const OPTIONAL_SCOPES = ['activity:read_all'];

    public const EVENT_REQUIRE_AUTHENTICATION = 'require_authentication';
    public const EVENT_REQUIRED_SCOPE_MISSING = 'required_scope_missing';
    public const EVENT_LOGIN_CANCELED = 'login_canceled';
    public const EVENT_CODE_EXCHANGE_FAILED = 'code_exchange_failed';

    public function __construct(
        private int $clientId,
        private string $clientSecret,
        private string $authUrl,
        private string $deauthUrl,
        private string $tokenUrl,
        private Routing $routing,
        private Client $client
    ) {
    }

    public function getLoginUrl(): string
    {
        $scopes = implode(',', [...self::REQUIRED_SCOPES, ...self::OPTIONAL_SCOPES]);
        $redirectUri = $this->routing->generateAbsoluteUrl('security.oauth');

        // #todo: build that in a cooler way
        return sprintf('%s?client_id=%u&redirect_uri=%s&response_type=code&scope=%s&approval_prompt=auto', $this->authUrl, $this->clientId, $redirectUri, $scopes);
    }

    public function checkScopes(array $requiredScopes, array $scopes): bool
    {
        return count(array_intersect($requiredScopes, $scopes)) === count($requiredScopes);
    }

    public function exchangeToken(string $code): stdClass
    {
        $response = $this->client->post($this->tokenUrl, ['query' => [
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'code' => $code,
            'grant_type' => 'authorization_code',
        ]]);

        return json_decode($response->getBody()->__toString());
    }

    public function refreshToken(string $refreshToken): stdClass
    {
        $response = $this->client->post($this->tokenUrl, ['query' => [
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'refresh_token' => $refreshToken,
            'grant_type' => 'refresh_token',
        ]]);

        return json_decode($response->getBody()->__toString());
    }

    public function deauthorize(string $accessToken): void
    {
        $this->client->post($this->deauthUrl, ['query' => [
            'access_token' => $accessToken,
        ]]);
    }
}
