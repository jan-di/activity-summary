<?php

declare(strict_types=1);

namespace App\Service;

use GuzzleHttp\Client;
use stdClass;

class StravaHook
{
    public const ASPECT_CREATE = 'create';
    public const ASPECT_UPDATE = 'update';
    public const ASPECT_DELETE = 'delete';

    public const OBJECT_ATHLETE = 'athlete';
    public const OBJECT_ACTIVITY = 'activity';

    public function __construct(
        private int $clientId,
        private string $clientSecret,
        private string $apiBaseUrl,
        private string $webhookToken,
        private Client $client
    ) {
    }

    public function viewSubscription(): array
    {
        $response = $this->client->get($this->apiBaseUrl.'/push_subscriptions', ['query' => [
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
        ]]);

        return json_decode($response->getBody()->__toString());
    }

    public function createSubscription(string $hookUrl): stdClass
    {
        $response = $this->client->post($this->apiBaseUrl.'/push_subscriptions', ['form_params' => [
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'verify_token' => $this->webhookToken,
            'callback_url' => $hookUrl,
        ]]);

        return json_decode($response->getBody()->__toString());
    }

    public function deleteSubscription(int $subscriptionId): void
    {
        $this->client->delete($this->apiBaseUrl.'/push_subscriptions/'.$subscriptionId, ['form_params' => [
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
        ]]);
    }

    public function getWebhookToken(): string
    {
        return $this->webhookToken;
    }

    public function verifyWebhookToken(string $webhookToken): bool
    {
        return hash_equals($this->webhookToken, $webhookToken);
    }

    public function createChallengeResponse(string $challengeToken): array
    {
        return [
            'hub.challenge' => $challengeToken,
        ];
    }
}
