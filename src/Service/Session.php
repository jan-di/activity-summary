<?php

declare(strict_types=1);

namespace App\Service;

class Session
{
    public function start(): void
    {
        session_start();

        if (!$this->isInitialized()) {
            $this->initialize();
        }
    }

    public function close(): void
    {
        session_write_close();
    }

    public function initialize(): void
    {
        $_SESSION['authenticated'] = false;
        $_SESSION['athlete_id'] = null;
        $_SESSION['athlete_name'] = null;
        $_SESSION['athlete_profile_url'] = null;
        $_SESSION['flash_message_text'] = null;
        $_SESSION['flash_message_class'] = null;
    }

    public function authenticate(int $athleteId, string $athleteName, string $athleteProfileUrl): void
    {
        $_SESSION['authenticated'] = true;
        $_SESSION['athlete_id'] = $athleteId;
        $_SESSION['athlete_name'] = $athleteName;
        $_SESSION['athlete_profile_url'] = $athleteProfileUrl;

        session_regenerate_id();
    }

    public function deauthenticate(): void
    {
        $this->initialize();

        session_regenerate_id();
    }

    public function isInitialized(): bool
    {
        return count($_SESSION) > 0;
    }

    public function isAuthenticated(): bool
    {
        return $_SESSION['authenticated'];
    }

    public function getAthleteId(): ?int
    {
        return $_SESSION['athlete_id'];
    }

    public function getAthleteName(): ?string
    {
        return $_SESSION['athlete_name'];
    }

    public function getAthleteProfileUrl(): ?string
    {
        return $_SESSION['athlete_profile_url'];
    }

    public function setFlashMessage(string $message, string $class = 'info'): void
    {
        $_SESSION['flash_message_text'] = $message;
        $_SESSION['flash_message_class'] = $class;
    }

    public function popFlashMessage(): ?array
    {
        $result = $_SESSION['flash_message_text'] !== null ? [
            'text' => $_SESSION['flash_message_text'],
            'class' => $_SESSION['flash_message_class'],
        ] : null;

        $_SESSION['flash_message_text'] = null;
        $_SESSION['flash_message_class'] = null;

        return $result;
    }
}
