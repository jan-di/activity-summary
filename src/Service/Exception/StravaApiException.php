<?php

declare(strict_types=1);

namespace App\Service\Exception;

use RuntimeException;
use Throwable;

class StravaApiException extends RuntimeException
{
    public function __construct(string $message, Throwable $previous)
    {
        parent::__construct($message, 0, $previous);
    }
}
