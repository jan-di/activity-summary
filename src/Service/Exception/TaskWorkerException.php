<?php

declare(strict_types=1);

namespace App\Service\Exception;

use RuntimeException;
use Throwable;

class TaskWorkerException extends RuntimeException
{
    public function __construct(string $message, Throwable $previous)
    {
        parent::__construct($message, 0, $previous);
    }
}
