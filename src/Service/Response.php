<?php

declare(strict_types=1);

namespace App\Service;

use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\Diactoros\Response\RedirectResponse;
use Laminas\Diactoros\Response\TextResponse;
use Negotiation\Accept;
use Negotiation\Negotiator;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Twig\Environment;

class Response
{
    public const TYPE_HTML = 'text/html';
    public const TYPE_JSON = 'application/json';
    public const TYPE_TEXT = 'text/plain';

    public const VALID_TYPES = [self::TYPE_HTML, self::TYPE_JSON, self::TYPE_TEXT];
    public const FALLBACK_TYPE = self::TYPE_TEXT;

    public function __construct(
        private Routing $routing,
        private Environment $twig,
        private Session $session,
        private Negotiator $negotiator,
    ) {
    }

    public function createTwigHtmlResponse(string $template, array $context = [], int $status = 200, ?ServerRequestInterface $request = null): ResponseInterface
    {
        $context = array_merge($context, [
            'session' => $this->session,
            'route' => $request?->getAttribute('_route', null),
        ]);

        $html = $this->twig->render($template, $context);

        return new HtmlResponse($html, $status);
    }

    public function getBestResponseType(RequestInterface $request): string
    {
        $header = $request->getHeaderLine('Accept');
        /** @var ?Accept $accept */
        $accept = $this->negotiator->getBest($header, self::VALID_TYPES);

        return $accept !== null ? $accept->getType() : self::FALLBACK_TYPE;
    }

    public function createErrorResponse(string $type, string $message, int $status): ResponseInterface
    {
        return match ($type) {
            self::TYPE_HTML => $this->createHtmlErrorResponse($message, $status),
            self::TYPE_JSON => $this->createJsonErrorResponse($message, $status),
            self::TYPE_TEXT => $this->createTextErrorResponse($message, $status)
        };
    }

    public function createHtmlErrorResponse(string $message, int $status): ResponseInterface
    {
        return $this->createTwigHtmlResponse('error.html.twig', [
            'error' => [
                'code' => $status,
                'message' => $message,
            ],
        ], $status);
    }

    public function createJsonErrorResponse(string $message, int $status): ResponseInterface
    {
        return new JsonResponse(['status' => $status, 'error' => $message], $status);
    }

    public function createTextErrorResponse(string $message, int $status): ResponseInterface
    {
        return new TextResponse(sprintf('%d %s', $status, $message), $status);
    }

    public function createRedirectResponse(string $route, array $parameters = []): ResponseInterface
    {
        $url = $this->routing->generateUrl($route, $parameters);

        return new RedirectResponse($url);
    }

    public function createExternalRedirectResponse(string $url): ResponseInterface
    {
        return new RedirectResponse($url);
    }

    public function createTableDisplayResponse(array $body, array $global = []): ResponseInterface
    {
        return new JsonResponse([
            'global' => $global,
            'data' => [
                'body' => $body,
            ],
        ]);
    }

    public function createChartDisplayResponse(array $series, array $global = []): ResponseInterface
    {
        return new JsonResponse([
            'global' => $global,
            'data' => [
                'series' => $series,
            ],
        ]);
    }
}
