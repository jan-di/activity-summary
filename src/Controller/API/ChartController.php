<?php

declare(strict_types=1);

namespace App\Controller\API;

use App\Display\AnnualCumulativeChart;
use App\Display\DeviceChart;
use App\Display\GearChart;
use App\Display\StartHeatmapChart;
use App\Service\ApiHelper;
use App\Service\Response;
use App\Service\Routing;
use App\Service\Session;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Routing\Annotation\Route;

class ChartController
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private Session $session,
        private ApiHelper $apiHelper,
        private ContainerInterface $container,
        private Response $response,
    ) {
    }

    #[Route(path: '/api/chart/start-heatmap', name: 'api.chart.start-heatmap', methods: ['get'], defaults: [
        Routing::REQUIRE_AUTHENTICATION => true,
    ])]
    public function startHeatmap(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $athleteId = $this->apiHelper->getAthleteId($request);
            $this->apiHelper->checkPermissionForAthleteId($athleteId);
            $commonActivityFilters = $this->apiHelper->getCommonActivityFilters($request);
        } catch (InvalidArgumentException $e) {
            return $this->apiHelper->createErrorResponse($request, $e);
        }

        /** @var StartHeatmapChart $display */
        $display = $this->container->get(StartHeatmapChart::class);
        $series = $display->getSeries($athleteId, $commonActivityFilters);

        return $this->response->createChartDisplayResponse($series);
    }

    #[Route(path: '/api/chart/annual-cumulative', name: 'api.chart.annual-cumulative', methods: ['get'], defaults: [
        Routing::REQUIRE_AUTHENTICATION => true,
    ])]
    public function annualCumulative(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $athleteId = $this->apiHelper->getAthleteId($request);
            $this->apiHelper->checkPermissionForAthleteId($athleteId);
            $commonActivityFilters = $this->apiHelper->getCommonActivityFilters($request);
            /** @var string $value */
            $value = $this->apiHelper->validateMatchValues($request, 'value', ['distance', 'movingTime'], allowNull: false);
        } catch (InvalidArgumentException $e) {
            return $this->apiHelper->createErrorResponse($request, $e);
        }

        /** @var AnnualCumulativeChart $display */
        $display = $this->container->get(AnnualCumulativeChart::class);
        $series = $display->getSeries($athleteId, $commonActivityFilters, $value);

        return $this->response->createChartDisplayResponse($series, [
            'value' => $value,
        ]);
    }

    #[Route(path: '/api/chart/devices', name: 'api.chart.devices', methods: ['get'], defaults: [
        Routing::REQUIRE_AUTHENTICATION => true,
    ])]
    public function devices(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $athleteId = $this->apiHelper->getAthleteId($request);
            $this->apiHelper->checkPermissionForAthleteId($athleteId);
            $commonActivityFilters = $this->apiHelper->getCommonActivityFilters($request);
        } catch (InvalidArgumentException $e) {
            return $this->apiHelper->createErrorResponse($request, $e);
        }

        /** @var DeviceChart $display */
        $display = $this->container->get(DeviceChart::class);
        $series = $display->getSeries($athleteId, $commonActivityFilters);

        return $this->response->createChartDisplayResponse($series);
    }

    #[Route(path: '/api/chart/gear', name: 'api.chart.gear', methods: ['get'], defaults: [
        Routing::REQUIRE_AUTHENTICATION => true,
    ])]
    public function gear(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $athleteId = $this->apiHelper->getAthleteId($request);
            $this->apiHelper->checkPermissionForAthleteId($athleteId);
            $commonActivityFilters = $this->apiHelper->getCommonActivityFilters($request);
            $gearType = $this->apiHelper->validateMatchValues($request, 'gear', ['bike', 'shoe']);
        } catch (InvalidArgumentException $e) {
            return $this->apiHelper->createErrorResponse($request, $e);
        }

        /** @var GearChart $display */
        $display = $this->container->get(GearChart::class);
        $series = $display->getSeries($athleteId, $commonActivityFilters, $gearType);

        return $this->response->createChartDisplayResponse($series);
    }
}
