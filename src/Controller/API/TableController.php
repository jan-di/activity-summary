<?php

declare(strict_types=1);

namespace App\Controller\API;

use App\Display\ActivityTable;
use App\Display\DeviceTable;
use App\Display\GearTable;
use App\Display\SummaryTable;
use App\Service\ApiHelper;
use App\Service\Response;
use App\Service\Routing;
use App\Service\Session;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Routing\Annotation\Route;

class TableController
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private Session $session,
        private ApiHelper $apiHelper,
        private ContainerInterface $container,
        private Response $response,
    ) {
    }

    #[Route(path: '/api/table/summary', name: 'api.table.summary', methods: ['get'], defaults: [
        Routing::REQUIRE_AUTHENTICATION => true,
    ])]
    public function summary(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $athleteId = $this->apiHelper->getAthleteId($request);
            $this->apiHelper->checkPermissionForAthleteId($athleteId);
            $commonActivityFilters = $this->apiHelper->getCommonActivityFilters($request);
        } catch (InvalidArgumentException $e) {
            return $this->apiHelper->createErrorResponse($request, $e);
        }

        /** @var SummaryTable $display */
        $display = $this->container->get(SummaryTable::class);
        $body = $display->getBody($athleteId, $commonActivityFilters);
        $data = $display->getGlobal();

        return $this->response->createTableDisplayResponse($body, $data);
    }

    #[Route(path: '/api/table/activities', name: 'api.table.activities', methods: ['get'], defaults: [
        Routing::REQUIRE_AUTHENTICATION => true,
    ])]
    public function activities(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $athleteId = $this->apiHelper->getAthleteId($request);
            $this->apiHelper->checkPermissionForAthleteId($athleteId);
            $commonActivityFilters = $this->apiHelper->getCommonActivityFilters($request);
        } catch (InvalidArgumentException $e) {
            return $this->apiHelper->createErrorResponse($request, $e);
        }

        /** @var ActivityTable $display */
        $display = $this->container->get(ActivityTable::class);
        $body = $display->getBody($athleteId, $commonActivityFilters);
        $data = $display->getGlobal();

        return $this->response->createTableDisplayResponse($body, $data);
    }

    #[Route(path: '/api/table/gear', name: 'api.table.gear', methods: ['get'], defaults: [
        Routing::REQUIRE_AUTHENTICATION => true,
    ])]
    public function gear(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $athleteId = $this->apiHelper->getAthleteId($request);
            $this->apiHelper->checkPermissionForAthleteId($athleteId);
            $commonActivityFilters = $this->apiHelper->getCommonActivityFilters($request);
            $gearType = $this->apiHelper->validateMatchValues($request, 'gear', ['bike', 'shoe']);
        } catch (InvalidArgumentException $e) {
            return $this->apiHelper->createErrorResponse($request, $e);
        }

        /** @var GearTable $display */
        $display = $this->container->get(GearTable::class);
        $body = $display->getBody($athleteId, $commonActivityFilters, $gearType);
        $data = $display->getGlobal();

        return $this->response->createTableDisplayResponse($body, $data);
    }

    #[Route(path: '/api/table/devices', name: 'api.table.devices', methods: ['get'], defaults: [
        Routing::REQUIRE_AUTHENTICATION => true,
    ])]
    public function devices(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $athleteId = $this->apiHelper->getAthleteId($request);
            $this->apiHelper->checkPermissionForAthleteId($athleteId);
            $commonActivityFilters = $this->apiHelper->getCommonActivityFilters($request);
        } catch (InvalidArgumentException $e) {
            return $this->apiHelper->createErrorResponse($request, $e);
        }

        /** @var DeviceTable $display */
        $display = $this->container->get(DeviceTable::class);
        $body = $display->getBody($athleteId, $commonActivityFilters);
        $data = $display->getGlobal();

        return $this->response->createTableDisplayResponse($body, $data);
    }
}
