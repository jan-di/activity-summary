<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\Response;
use App\Service\Session;
use App\Service\StravaAuth;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Routing\Annotation\Route;

class IndexController
{
    public function __construct(
        private Response $response,
        private Session $session,
        private StravaAuth $stravaAuth,
    ) {
    }

    #[Route(path: '/', name: 'index', methods: ['get'])]
    public function index(): ResponseInterface
    {
        if ($this->session->isAuthenticated()) {
            return $this->response->createRedirectResponse('content.overview');
        }

        return $this->response->createTwigHtmlResponse('index.html.twig', [
            'loginUrl' => $this->stravaAuth->getLoginUrl(),
        ]);
    }
}
