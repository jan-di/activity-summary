<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\Response;
use App\Service\Routing;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController
{
    public function __construct(private Response $responseFactory)
    {
    }

    #[Route(path: '/profile', name: 'profile', methods: ['get'], defaults: [
        Routing::REQUIRE_AUTHENTICATION => true,
    ])]
    public function index(ServerRequestInterface $request): ResponseInterface
    {
        return $this->responseFactory->createTwigHtmlResponse('profile.html.twig');
    }
}
