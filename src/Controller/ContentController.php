<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\ApiHelper;
use App\Service\Response;
use App\Service\Routing;
use App\Service\Session;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Routing\Annotation\Route;

class ContentController
{
    public function __construct(
        private Response $responseFactory,
        private ApiHelper $activityFilter,
        private Session $session
    ) {
    }

    #[Route(path: '/overview', name: 'content.overview', methods: ['get'], defaults: [
        Routing::REQUIRE_AUTHENTICATION => true,
    ])]
    public function overview(ServerRequestInterface $request): ResponseInterface
    {
        /** @var int $athleteId */
        $athleteId = $this->session->getAthleteId();
        $filterOptions = $this->activityFilter->getFilterOptions($athleteId);

        return $this->responseFactory->createTwigHtmlResponse('content/overview.html.twig', [
            'activityFilterOptions' => $filterOptions,
        ], request: $request);
    }

    #[Route(path: '/list', name: 'content.list', methods: ['get'], defaults: [
        Routing::REQUIRE_AUTHENTICATION => true,
    ])]
    public function list(ServerRequestInterface $request): ResponseInterface
    {
        /** @var int $athleteId */
        $athleteId = $this->session->getAthleteId();
        $filterOptions = $this->activityFilter->getFilterOptions($athleteId);

        return $this->responseFactory->createTwigHtmlResponse('content/list.html.twig', [
            'activityFilterOptions' => $filterOptions,
        ], request: $request);
    }

    #[Route(path: '/gear', name: 'content.gear', methods: ['get'], defaults: [
        Routing::REQUIRE_AUTHENTICATION => true,
    ])]
    public function gear(ServerRequestInterface $request): ResponseInterface
    {
        /** @var int $athleteId */
        $athleteId = $this->session->getAthleteId();
        $filterOptions = $this->activityFilter->getFilterOptions($athleteId);

        return $this->responseFactory->createTwigHtmlResponse('content/gear.html.twig', [
            'activityFilterOptions' => $filterOptions,
        ], request: $request);
    }
}
