<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\MessageQueue;
use App\Service\Response;
use App\Service\StravaHook;
use App\Task\DeleteActivityTask;
use App\Task\DeleteAthleteTask;
use App\Task\FetchActivityDetailsTask;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\Annotation\Route;

class WebhookController
{
    public function __construct(
        private Response $responseFactory,
        private StravaHook $stravaHook,
        private MessageQueue $messageQueue,
        private LoggerInterface $logger,
    ) {
    }

    /**
     * @see https://developers.strava.com/docs/webhooks/#event-data
     */
    #[Route(path: '/hook/{token}', name: 'webhook.event', methods: ['post'])]
    public function event(ServerRequestInterface $request): ResponseInterface
    {
        $webhookToken = $request->getAttribute('token');
        if (!$this->stravaHook->verifyWebhookToken($webhookToken)) {
            $this->logger->error('Received Webhook Event with invalid token.', [
                'token' => $webhookToken,
            ]);

            return new JsonResponse(['error' => 'invalid verify token'], 400);
        }
        $event = json_decode($request->getBody()->__toString());

        $ownerId = $event->owner_id ?? null;
        $objectId = $event->object_id ?? null;
        $objectType = $event->object_type ?? null;
        $aspectType = $event->aspect_type ?? null;
        $updates = $event->updates ?? null;

        if ($ownerId === null) {
            $task = ['error' => 'missing owner_id'];
        } elseif ($objectId === null) {
            $task = ['error' => 'missing object_id'];
        } else {
            if ($objectType === StravaHook::OBJECT_ACTIVITY) {
                $task = match ($aspectType) {
                    StravaHook::ASPECT_CREATE, StravaHook::ASPECT_UPDATE => FetchActivityDetailsTask::create($ownerId, $objectId),
                    StravaHook::ASPECT_DELETE => DeleteActivityTask::create($objectId),
                    default => ['error' => 'invalid aspect_type for activity object']
                };
            } elseif ($objectType === StravaHook::OBJECT_ATHLETE) {
                if ($aspectType === StravaHook::ASPECT_UPDATE && $updates !== null && ($updates->authorized ?? null) === 'false') {
                    $task = DeleteAthleteTask::create($ownerId);
                } else {
                    $task = ['error' => 'invalid webhook message for athlete object'];
                }
            } else {
                $task = ['error' => 'invalid object_type'];
            }
        }

        if (isset($task['error'])) {
            $this->logger->warning('Received invalid Webhook Event', [
                'body' => $event,
                'error' => $task['error'],
            ]);

            return new JsonResponse($task);
        }

        $this->logger->info('Received Webhook Event', [
            'body' => $event,
            'task' => $task,
        ]);

        $this->messageQueue->publish(json_encode($task), 1);
        $this->messageQueue->close();

        return new JsonResponse('ok');
    }

    /**
     * @see https://developers.strava.com/docs/webhooks/#subscription-validation-request
     */
    #[Route(path: '/hook/{token}', name: 'webhook.validation', methods: ['get'])]
    public function validation(ServerRequestInterface $request): ResponseInterface
    {
        $hubMode = $request->getQueryParams()['hub_mode'] ?? null;
        $hubChallenge = $request->getQueryParams()['hub_challenge'] ?? null;
        $hubVerifyToken = $request->getQueryParams()['hub_verify_token'] ?? null;

        if ($hubMode !== 'subscribe' || $hubChallenge === null || $hubVerifyToken === null) {
            return new JsonResponse(['error' => 'bad request'], 400);
        }

        if (!$this->stravaHook->verifyWebhookToken($hubVerifyToken)) {
            return new JsonResponse(['error' => 'invalid verify token'], 400);
        }

        return new JsonResponse($this->stravaHook->createChallengeResponse($hubChallenge));
    }
}
