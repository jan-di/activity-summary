<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Athlete;
use App\Service\MessageQueue;
use App\Service\Response;
use App\Service\Session;
use App\Service\StravaAuth;
use App\Task\DeleteAthleteTask;
use App\Task\FetchAllActivitiesTask;
use DateTimeImmutable;
use Doctrine\ORM\EntityManager;
use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController
{
    public function __construct(
        private Response $responseFactory,
        private StravaAuth $stravaAuth,
        private EntityManager $entityManager,
        private Session $session,
        private MessageQueue $messageQueue,
    ) {
    }

    #[Route(path: '/login', name: 'security.login', methods: ['GET'])]
    public function login(ServerRequestInterface $request): ResponseInterface
    {
        // redirect to index, if user is already logged in
        if ($this->session->isAuthenticated()) {
            return $this->responseFactory->createRedirectResponse('content.overview');
        }

        $errorMessage = match ($request->getQueryParams()['event'] ?? '') {
            StravaAuth::EVENT_REQUIRE_AUTHENTICATION => 'You must login to access this page',
            StravaAuth::EVENT_REQUIRED_SCOPE_MISSING => 'You must at least allow access to profile and public activities to use this application!',
            StravaAuth::EVENT_LOGIN_CANCELED => null,
            StravaAuth::EVENT_CODE_EXCHANGE_FAILED => 'Login failed. Please try again.',
            default => null
        };

        return $this->responseFactory->createTwigHtmlResponse('login.html.twig', [
            'loginUrl' => $this->stravaAuth->getLoginUrl(),
            'errorMessage' => $errorMessage,
        ]);
    }

    #[Route(path: '/logout', name: 'security.logout', methods: ['POST'])]
    public function logout(ServerRequestInterface $request): ResponseInterface
    {
        $delete = ($request->getParsedBody()['delete'] ?? null) === '1';

        if ($this->session->isAuthenticated()) {
            if ($delete) {
                /** @var int $athleteId */
                $athleteId = $this->session->getAthleteId();
                $athleteRepository = $this->entityManager->getRepository(Athlete::class);
                /** @var Athlete $athlete */
                $athlete = $athleteRepository->find($athleteId);
                $this->stravaAuth->deauthorize($athlete->getAccessToken() ?? '');

                $task = DeleteAthleteTask::create($athleteId);
                $this->messageQueue->publish(json_encode($task), DeleteAthleteTask::getTaskPriority());
            }
            $this->session->deauthenticate();
        }

        // redirect to index page after successful logout
        return $this->responseFactory->createRedirectResponse('index');
    }

    #[Route(path: '/oauth', name: 'security.oauth', methods: ['GET'])]
    public function oauth(ServerRequestInterface $request): ResponseInterface
    {
        // check if user has cacneled login
        if (($request->getQueryParams()['error'] ?? '') === 'access_denied') {
            return $this->responseFactory->createRedirectResponse('security.login', [
                'event' => StravaAuth::EVENT_LOGIN_CANCELED,
            ]);
        }

        // check for required and optional scopes
        $scopes = explode(',', $request->getQueryParams()['scope'] ?? '');
        if (!$this->stravaAuth->checkScopes(StravaAuth::REQUIRED_SCOPES, $scopes)) {
            return $this->responseFactory->createRedirectResponse('security.login', [
                'event' => StravaAuth::EVENT_REQUIRED_SCOPE_MISSING,
            ]);
        }
        $private = $this->stravaAuth->checkScopes(StravaAuth::OPTIONAL_SCOPES, $scopes);

        // exchange code for access token
        try {
            $response = $this->stravaAuth->exchangeToken($request->getQueryParams()['code']);
        } catch (Exception) {
            return $this->responseFactory->createRedirectResponse('security.login', [
                'event' => StravaAuth::EVENT_CODE_EXCHANGE_FAILED,
            ]);
        }

        // create or update athlete
        $athleteId = $response->athlete->id;
        $athleteRepository = $this->entityManager->getRepository(Athlete::class);

        /** @var ?Athlete $athlete */
        $athlete = $athleteRepository->find($athleteId);

        if ($athlete === null) {
            $firstLogin = true;
            $athlete = new Athlete($response->athlete);
        } else {
            $firstLogin = false;
        }

        $athlete
            ->setAccessToken($response->access_token)
            ->setRefreshToken($response->refresh_token)
            ->setTokenExpiresAt((new DateTimeImmutable())->setTimestamp($response->expires_at))
            ->setPrivateAccess($private);

        $this->entityManager->persist($athlete);
        $this->entityManager->flush();

        if ($firstLogin) {
            $task = FetchAllActivitiesTask::create($athlete->getId());
            $this->messageQueue->publish(json_encode($task), FetchAllActivitiesTask::getTaskPriority());
        }

        // authenticate session
        $this->session->authenticate($athlete->getId(), $athlete->getName(), $athlete->getProfileUrl());

        if ($firstLogin) {
            $this->session->setFlashMessage('You have successfully connected with your strava account.
                The initial synchronisation has been started. Please come back in a few minutes to see your personal activity summary!', 'success');
        }

        // redirect to index page after successful login
        return $this->responseFactory->createRedirectResponse('content.overview');
    }
}
