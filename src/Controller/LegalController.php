<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\Response;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Routing\Annotation\Route;

class LegalController
{
    public function __construct(
        private Response $response
    ) {
    }

    #[Route(path: '/legal', name: 'legal.disclosure', methods: ['get'])]
    public function index(): ResponseInterface
    {
        return $this->response->createTwigHtmlResponse('legal/legal.html.twig');
    }

    #[Route(path: '/privacy', name: 'legal.privacy', methods: ['get'])]
    public function privacy(): ResponseInterface
    {
        return $this->response->createTwigHtmlResponse('legal/privacy.html.twig');
    }
}
