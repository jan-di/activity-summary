<?php

declare(strict_types=1);

namespace App\Display;

use App\Service\ApiHelper;
use Doctrine\ORM\EntityManagerInterface;

class DeviceChart extends AbstractDisplay
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private ApiHelper $apiHelper
    ) {
    }

    public function getSeries(int $athleteId, array $commonActivityFilters): array
    {
        $sqlClauses = [];
        $sqlParameters = [];

        $this->apiHelper->applyActivityAthleteFilter($sqlClauses, $sqlParameters, $athleteId);
        $this->apiHelper->applyCommonActivityFilters($sqlClauses, $sqlParameters, $commonActivityFilters);
        $sqlWhereClause = $this->apiHelper->buildWhereClause($sqlClauses);

        $connection = $this->entityManager->getConnection();
        $stmt = $connection->prepare("SELECT
            COALESCE(activity.device_name, '(No Device)') AS name,
            COUNT(activity.id) AS y
            FROM activity
            WHERE $sqlWhereClause
            GROUP BY activity.device_name");
        $stmt->execute($sqlParameters);

        return [
            ['data' => $stmt->fetchAllAssociative()],
        ];
    }
}
