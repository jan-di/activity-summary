<?php

declare(strict_types=1);

namespace App\Display;

use App\Service\ApiHelper;
use Doctrine\ORM\EntityManagerInterface;

class ActivityTable extends AbstractDisplay
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private ApiHelper $apiHelper,
        private string $activityTemplateUrl
    ) {
    }

    public function getGlobal(): array
    {
        return [
            'activity_url' => $this->activityTemplateUrl,
        ];
    }

    public function getBody(int $athleteId, array $commonActivityFilters): array
    {
        $sqlClauses = [];
        $sqlParameters = [];

        $this->apiHelper->applyActivityAthleteFilter($sqlClauses, $sqlParameters, $athleteId);
        $this->apiHelper->applyCommonActivityFilters($sqlClauses, $sqlParameters, $commonActivityFilters);
        $sqlWhereClause = $this->apiHelper->buildWhereClause($sqlClauses);

        $connection = $this->entityManager->getConnection();
        $stmt = $connection->prepare("SELECT 
                activity.id,
                activity.type,
                activity.name,
                DATE_ADD(activity.start_date, INTERVAL activity.utc_offset SECOND) AS local_start_date,
                activity.distance,
                activity.elevation_gain,
                activity.moving_time,
                activity.private,
                activity.commute,
                activity.description,
                activity.device_name,
                gear.id AS gear_id,
                gear.name AS gear_name
            FROM activity
            LEFT JOIN gear ON activity.gear_id = gear.id
            WHERE $sqlWhereClause
            ORDER BY local_start_date DESC");
        $stmt->execute($sqlParameters);

        return $stmt->fetchAllAssociative();
    }
}
