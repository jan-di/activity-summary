<?php

declare(strict_types=1);

namespace App\Display;

use App\Service\ApiHelper;
use Doctrine\ORM\EntityManagerInterface;

class GearChart extends AbstractDisplay
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private ApiHelper $apiHelper
    ) {
    }

    public function getSeries(int $athleteId, array $commonActivityFilters, ?string $gearType): array
    {
        $sqlClauses = [];
        $sqlParameters = [];

        $this->apiHelper->applyActivityAthleteFilter($sqlClauses, $sqlParameters, $athleteId);
        $this->apiHelper->applyCommonActivityFilters($sqlClauses, $sqlParameters, $commonActivityFilters);
        $this->apiHelper->addMatchClause($sqlClauses, $gearType, [
            'shoe' => 'gear.id LIKE "g%"',
            'bike' => 'gear.id LIKE "b%"',
        ]);
        $sqlWhereClause = $this->apiHelper->buildWhereClause($sqlClauses);

        $connection = $this->entityManager->getConnection();
        $stmt = $connection->prepare("SELECT
                gear.name,
                COUNT(activity.id) AS y
            FROM gear
            LEFT JOIN activity ON gear.id = activity.gear_id
            WHERE $sqlWhereClause
            GROUP BY gear.id");
        $stmt->execute($sqlParameters);

        return [
            ['data' => $stmt->fetchAllAssociative()],
        ];
    }
}
