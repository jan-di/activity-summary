<?php

declare(strict_types=1);

namespace App\Display;

use App\Service\ApiHelper;
use DateInterval;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;

class AnnualCumulativeChart extends AbstractDisplay
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private ApiHelper $apiHelper
    ) {
    }

    public function getSeries(int $athleteId, array $commonActivityFilters, string $value): array
    {
        $sqlClauses = [];
        $sqlParameters = [];

        $this->apiHelper->applyActivityAthleteFilter($sqlClauses, $sqlParameters, $athleteId);
        $this->apiHelper->applyCommonActivityFilters($sqlClauses, $sqlParameters, $commonActivityFilters);
        $sqlWhereClause = $this->apiHelper->buildWhereClause($sqlClauses);

        $valueField = match ($value) {
            'distance' => 'distance',
            'movingTime' => 'moving_time'
        };

        $connection = $this->entityManager->getConnection();
        $stmt = $connection->prepare("SELECT
                YEAR(local_start) AS year,
                local_start,
                value,
                ROUND(SUM(value) OVER (PARTITION BY YEAR(local_start) ORDER BY local_start), 2) AS running_value
            FROM (
                SELECT 
                    DATE_ADD(start_date, INTERVAL utc_offset SECOND) AS local_start, 
                    $valueField AS value
                FROM activity
                WHERE $sqlWhereClause
            ) AS sub
            ORDER BY YEAR, local_start
        ");
        $stmt->execute($sqlParameters);

        $result = [];
        foreach ($stmt->fetchAllAssociative() as $row) {
            if (!isset($result[$row['year']])) {
                $result[$row['year']] = [];
                $result[$row['year']]['name'] = $row['year'];
                $result[$row['year']]['data'] = [];
            }

            $result[$row['year']]['data'][] = [
                'x' => $this->getNormalizedTimestamp(new DateTimeImmutable($row['local_start'])),
                'y' => (float) $row['running_value'],
                'custom' => [
                    'value' => (float) $row['value'],
                    'start' => $row['local_start'],
                ],
            ];
        }

        return array_values($result);
    }

    private function getNormalizedTimestamp(DateTimeInterface $dateTime): int
    {
        $dateTime = DateTime::createFromInterface($dateTime);
        $year = intval($dateTime->format('Y'));

        $dateTime->sub(new DateInterval(sprintf('P%uY', abs($year - 1972))));

        return intval($dateTime->format('U')) * 1000;
    }
}
