<?php

declare(strict_types=1);

namespace App\Display;

use App\Service\ApiHelper;
use Doctrine\ORM\EntityManagerInterface;

class SummaryTable extends AbstractDisplay
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private ApiHelper $apiHelper
    ) {
    }

    public function getBody(int $athleteId, array $commonActivityFilters): array
    {
        $sqlClauses = [];
        $sqlParameters = [];

        $this->apiHelper->applyActivityAthleteFilter($sqlClauses, $sqlParameters, $athleteId);
        $this->apiHelper->applyCommonActivityFilters($sqlClauses, $sqlParameters, $commonActivityFilters);
        $sqlWhereClause = $this->apiHelper->buildWhereClause($sqlClauses);

        $connection = $this->entityManager->getConnection();
        $stmt = $connection->prepare("SELECT
                activity.type,
                COUNT(activity.id) AS total_activities,
                COUNT(IF(activity.commute, 1, NULL)) AS commute_activities,
                COUNT(IF(activity.private, 1, NULL)) AS private_activities,
                SUM(activity.distance) AS total_distance,
                ROUND(AVG(activity.distance), 2) AS average_distance,
                SUM(activity.elevation_gain) AS total_elevation,
                ROUND(AVG(activity.elevation_gain), 2) AS average_elevation,
                SUM(activity.moving_time) AS total_moving_time,
                ROUND(AVG(activity.moving_time)) AS average_moving_time
            FROM activity
            WHERE $sqlWhereClause
            GROUP BY activity.type ASC WITH ROLLUP");
        $stmt->execute($sqlParameters);

        return $stmt->fetchAllAssociative();
    }
}
