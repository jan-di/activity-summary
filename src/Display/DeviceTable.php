<?php

declare(strict_types=1);

namespace App\Display;

use App\Service\ApiHelper;
use Doctrine\ORM\EntityManagerInterface;

class DeviceTable extends AbstractDisplay
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private ApiHelper $apiHelper
    ) {
    }

    public function getBody(int $athleteId, array $commonActivityFilters): array
    {
        $sqlClauses = [];
        $sqlParameters = [];

        $this->apiHelper->applyActivityAthleteFilter($sqlClauses, $sqlParameters, $athleteId);
        $this->apiHelper->applyCommonActivityFilters($sqlClauses, $sqlParameters, $commonActivityFilters);
        $sqlWhereClause = $this->apiHelper->buildWhereClause($sqlClauses);

        $connection = $this->entityManager->getConnection();
        $stmt = $connection->prepare("SELECT
                activity.device_name AS name,
                COUNT(activity.id) AS activity_count,
                SUM(activity.distance) AS total_distance,
                SUM(activity.moving_time) AS total_time
            FROM activity
            WHERE $sqlWhereClause
            GROUP BY activity.device_name");
        $stmt->execute($sqlParameters);

        return $stmt->fetchAllAssociative();
    }
}
