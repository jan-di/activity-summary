<?php

declare(strict_types=1);

namespace App\Display;

use App\Service\ApiHelper;
use Doctrine\ORM\EntityManagerInterface;

class GearTable extends AbstractDisplay
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private ApiHelper $apiHelper
    ) {
    }

    public function getBody(int $athleteId, array $commonActivityFilters, ?string $gearType): array
    {
        $sqlClauses = [];
        $sqlParameters = [];

        $this->apiHelper->applyActivityAthleteFilter($sqlClauses, $sqlParameters, $athleteId);
        $this->apiHelper->applyCommonActivityFilters($sqlClauses, $sqlParameters, $commonActivityFilters);
        $this->apiHelper->addMatchClause($sqlClauses, $gearType, [
            'shoe' => 'gear.id LIKE "g%"',
            'bike' => 'gear.id LIKE "b%"',
        ]);
        $sqlWhereClause = $this->apiHelper->buildWhereClause($sqlClauses);

        $connection = $this->entityManager->getConnection();
        $stmt = $connection->prepare("SELECT
                gear.id,
                gear.name,
                gear.`primary`,
                COUNT(activity.id) AS activity_count,
                SUM(activity.distance) AS total_distance,
                SUM(activity.moving_time) AS total_time
            FROM gear
            LEFT JOIN activity ON gear.id = activity.gear_id
            WHERE $sqlWhereClause
            GROUP BY gear.id");
        $stmt->execute($sqlParameters);

        return $stmt->fetchAllAssociative();
    }
}
