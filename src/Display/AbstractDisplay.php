<?php

declare(strict_types=1);

namespace App\Display;

class AbstractDisplay
{
    public function getGlobal(): array
    {
        return [];
    }
}
