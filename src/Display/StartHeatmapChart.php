<?php

declare(strict_types=1);

namespace App\Display;

use App\Service\ApiHelper;
use Doctrine\ORM\EntityManagerInterface;

class StartHeatmapChart extends AbstractDisplay
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private ApiHelper $apiHelper
    ) {
    }

    public function getSeries(int $athleteId, array $commonActivityFilters): array
    {
        $sqlClauses = [];
        $sqlParameters = [];

        $this->apiHelper->applyActivityAthleteFilter($sqlClauses, $sqlParameters, $athleteId);
        $this->apiHelper->applyCommonActivityFilters($sqlClauses, $sqlParameters, $commonActivityFilters);
        $sqlWhereClause = $this->apiHelper->buildWhereClause($sqlClauses);

        $connection = $this->entityManager->getConnection();
        $stmt = $connection->prepare("SELECT 
                HOUR(local_start) AS HOUR,
                WEEKDAY(local_start) AS WEEKDAY,
                COUNT(id) AS COUNT
            FROM (
                SELECT id, DATE_ADD(start_date, INTERVAL utc_offset SECOND) AS local_start FROM activity 
                WHERE $sqlWhereClause
            ) AS X
            GROUP BY hour, weekday");
        $stmt->execute($sqlParameters);

        return [
            ['data' => $stmt->fetchAllNumeric()],
        ];
    }
}
