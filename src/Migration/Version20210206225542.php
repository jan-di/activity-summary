<?php

declare(strict_types=1);

namespace App\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210206225542 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE activity (id BIGINT UNSIGNED NOT NULL, athlete_id BIGINT UNSIGNED DEFAULT NULL, gear_id VARCHAR(20) DEFAULT NULL, detailed TINYINT(1) NOT NULL, type VARCHAR(30) NOT NULL, name VARCHAR(255) NOT NULL, distance DOUBLE PRECISION UNSIGNED NOT NULL, elevation_gain DOUBLE PRECISION UNSIGNED NOT NULL, moving_time INT UNSIGNED NOT NULL, private TINYINT(1) NOT NULL, commute TINYINT(1) NOT NULL, start_date DATETIME NOT NULL, utc_offset INT NOT NULL, description LONGTEXT DEFAULT NULL, device_name VARCHAR(100) DEFAULT NULL, INDEX IDX_AC74095AFE6BCB8B (athlete_id), INDEX IDX_AC74095A77201934 (gear_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gear (id VARCHAR(20) NOT NULL, detailed TINYINT(1) NOT NULL, name VARCHAR(150) NOT NULL, `primary` TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE activity ADD CONSTRAINT FK_AC74095AFE6BCB8B FOREIGN KEY (athlete_id) REFERENCES athlete (id)');
        $this->addSql('ALTER TABLE activity ADD CONSTRAINT FK_AC74095A77201934 FOREIGN KEY (gear_id) REFERENCES gear (id)');
        $this->addSql('ALTER TABLE athlete ADD detailed TINYINT(1) NOT NULL, CHANGE private_access private_access TINYINT(1) DEFAULT NULL, CHANGE access_token access_token CHAR(40) DEFAULT NULL, CHANGE refresh_token refresh_token CHAR(40) DEFAULT NULL, CHANGE token_expires_at token_expires_at DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE activity DROP FOREIGN KEY FK_AC74095A77201934');
        $this->addSql('DROP TABLE activity');
        $this->addSql('DROP TABLE gear');
        $this->addSql('ALTER TABLE athlete DROP detailed, CHANGE private_access private_access TINYINT(1) NOT NULL, CHANGE access_token access_token VARCHAR(40) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE refresh_token refresh_token VARCHAR(40) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE token_expires_at token_expires_at DATETIME NOT NULL');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
