<?php

declare(strict_types=1);

namespace App\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210131140802 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE athlete (id BIGINT UNSIGNED NOT NULL, firstname VARCHAR(100) NOT NULL, lastname VARCHAR(100) NOT NULL, profile_url VARCHAR(120) NOT NULL, private_access TINYINT(1) NOT NULL, access_token VARCHAR(40) NOT NULL, refresh_token VARCHAR(40) NOT NULL, token_expires_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_C03B8321B6A2DD68 (access_token), UNIQUE INDEX UNIQ_C03B8321C74F2195 (refresh_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE athlete');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
