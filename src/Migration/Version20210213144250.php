<?php

declare(strict_types=1);

namespace App\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210213144250 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE activity CHANGE athlete_id athlete_id BIGINT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE gear ADD athlete_id BIGINT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE gear ADD CONSTRAINT FK_B44539BFE6BCB8B FOREIGN KEY (athlete_id) REFERENCES athlete (id)');
        $this->addSql('CREATE INDEX IDX_B44539BFE6BCB8B ON gear (athlete_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE activity CHANGE athlete_id athlete_id BIGINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE gear DROP FOREIGN KEY FK_B44539BFE6BCB8B');
        $this->addSql('DROP INDEX IDX_B44539BFE6BCB8B ON gear');
        $this->addSql('ALTER TABLE gear DROP athlete_id');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
