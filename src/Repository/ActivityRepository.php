<?php

declare(strict_types=1);

namespace App\Repository;

class ActivityRepository extends AbstractRepository
{
    public function deleteByAthleteId(int $athleteId): void
    {
        $activityIds = $this->executeDql('SELECT act.id FROM E:Activity act JOIN act.athlete ath WHERE ath.id = :athleteId', [
            'athleteId' => $athleteId,
        ]);
        $this->executeDql('DELETE E:Activity act WHERE act.id IN (:ids)', [
            'ids' => $activityIds,
        ]);
    }
}
