<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

abstract class AbstractRepository extends EntityRepository
{
    public function executeDql(string $dql, array $parameters): mixed
    {
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters($parameters);

        return $query->execute();
    }
}
