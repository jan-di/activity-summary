<?php

declare(strict_types=1);

namespace App\Repository;

class GearRepository extends AbstractRepository
{
    public function deleteByAthleteId(int $athleteId): void
    {
        $activityIds = $this->executeDql('SELECT g.id FROM E:Gear g JOIN g.athlete ath WHERE ath.id = :athleteId', [
            'athleteId' => $athleteId,
        ]);
        $this->executeDql('DELETE E:Gear g WHERE g.id IN (:ids)', [
            'ids' => $activityIds,
        ]);
    }
}
