<?php

declare(strict_types=1);

namespace App\Repository;

use DateInterval;
use DateTime;

class AthleteRepository extends AbstractRepository
{
    public function findAthletesForTokenRefresh(): array
    {
        $filterDateTime = (new DateTime('now'))->add(new DateInterval('PT3600S'));

        return $this->executeDql('SELECT a FROM E:Athlete a WHERE a.tokenExpiresAt < :datetime', [
            'datetime' => $filterDateTime,
        ]);
    }
}
