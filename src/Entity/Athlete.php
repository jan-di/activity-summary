<?php

declare(strict_types=1);

namespace App\Entity;

use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=App\Repository\AthleteRepository::class)
 * @ORM\Table(name="athlete", options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"})
 */
class Athlete extends AbstractStravaEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="bigint", options={"unsigned"=true})
     */
    protected int $id;

    /**
     * @ORM\Column(name="firstname", type="string", length=100)
     */
    protected string $firstname;

    /**
     * @ORM\Column(name="lastname", type="string", length=100)
     */
    protected string $lastname;

    /**
     * @ORM\Column(name="profile_url", type="string", length=200)
     */
    protected string $profileUrl;

    /**
     * @ORM\Column(name="private_access", type="boolean", nullable=true)
     */
    protected ?bool $privateAccess = null;

    /**
     * @ORM\Column(name="access_token", type="string", length=40, unique=true, nullable=true, options={"fixed"=true})
     */
    protected ?string $accessToken = null;

    /**
     * @ORM\Column(name="refresh_token", type="string", length=40, unique=true, nullable=true, options={"fixed"=true})
     */
    protected ?string $refreshToken = null;

    /**
     * @ORM\Column(name="token_expires_at", type="datetime", nullable=true)
     */
    protected ?DateTimeInterface $tokenExpiresAt;

    public function __construct(\stdClass $athleteResource)
    {
        $this->id = $athleteResource->id;

        $this->update($athleteResource);

        $this->tokenExpiresAt = new DateTimeImmutable();
    }

    public function update(\stdClass $athleteResource): self
    {
        $this->setDetailed($athleteResource->resource_state);

        $this->firstname = $athleteResource->firstname;
        $this->lastname = $athleteResource->lastname;
        $this->profileUrl = $athleteResource->profile;

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function getName(): string
    {
        return $this->firstname.' '.$this->lastname;
    }

    public function getProfileUrl(): string
    {
        return $this->profileUrl;
    }

    public function isPrivateAccess(): ?bool
    {
        return $this->privateAccess;
    }

    public function setPrivateAccess(?bool $private): self
    {
        $this->privateAccess = $private;

        return $this;
    }

    public function getAccessToken(): ?string
    {
        return $this->accessToken;
    }

    public function setAccessToken(?string $token): self
    {
        $this->accessToken = $token;

        return $this;
    }

    public function getRefreshToken(): ?string
    {
        return $this->refreshToken;
    }

    public function setRefreshToken(?string $token): self
    {
        $this->refreshToken = $token;

        return $this;
    }

    public function getTokenExpiresAt(): ?DateTimeInterface
    {
        return $this->tokenExpiresAt;
    }

    public function setTokenExpiresAt(?DateTimeInterface $expiresAt): self
    {
        $this->tokenExpiresAt = $expiresAt;

        return $this;
    }
}
