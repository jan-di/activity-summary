<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=App\Repository\GearRepository::class)
 * @ORM\Table(name="gear", options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"})
 */
class Gear extends AbstractStravaEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="string", length=20)
     */
    protected string $id;

    /**
     * @ORM\ManyToOne(targetEntity="Athlete")
     * @ORM\JoinColumn(name="athlete_id", referencedColumnName="id", nullable=false)
     */
    protected Athlete $athlete;

    /**
     * @ORM\Column(name="name", type="string", length=150)
     */
    protected string $name;

    /**
     * @ORM\Column(name="`primary`", type="boolean")
     */
    protected bool $primary;

    public function __construct(\stdClass $gearResource, Athlete $athlete)
    {
        $this->id = $gearResource->id;
        $this->athlete = $athlete;

        $this->update($gearResource);
    }

    public function update(\stdClass $gearResource): self
    {
        $this->setDetailed($gearResource->resource_state);

        $this->name = $gearResource->name;
        $this->primary = $gearResource->primary;

        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getAthlete(): Athlete
    {
        return $this->athlete;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
