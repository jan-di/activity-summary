<?php

declare(strict_types=1);

namespace App\Entity;

use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=App\Repository\ActivityRepository::class)
 * @ORM\Table(name="activity", options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"})
 */
class Activity extends AbstractStravaEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="bigint", options={"unsigned"=true})
     */
    protected int $id;

    /**
     * @ORM\ManyToOne(targetEntity="Athlete")
     * @ORM\JoinColumn(name="athlete_id", referencedColumnName="id", nullable=false)
     */
    protected Athlete $athlete;

    /**
     * @ORM\Column(name="type", type="string", length=30)
     */
    protected string $type;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected string $name;

    /**
     * @ORM\Column(name="distance", type="float", options={"unsigned"=true})
     */
    protected float $distance;

    /**
     * @ORM\Column(name="elevation_gain", type="float", options={"unsigned"=true})
     */
    protected float $elevationGain;

    /**
     * @ORM\Column(name="moving_time", type="integer", options={"unsigned"=true})
     */
    protected int $movingTime;

    /**
     * @ORM\Column(name="private", type="boolean")
     */
    protected bool $private;

    /**
     * @ORM\Column(name="commute", type="boolean")
     */
    protected bool $commute;

    /**
     * @ORM\Column(name="start_date", type="datetime")
     */
    protected DateTimeInterface $startDate;

    /**
     * @ORM\Column(name="utc_offset", type="integer")
     */
    protected int $utcOffset;

    /**
     * @ORM\ManyToOne(targetEntity="Gear")
     * @ORM\JoinColumn(name="gear_id", referencedColumnName="id")
     */
    protected ?Gear $gear = null;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected ?string $description = null;

    /**
     * @ORM\Column(name="device_name", type="string", length=100, nullable=true)
     */
    protected ?string $deviceName = null;

    public function __construct(\stdClass $activityResource, Athlete $athlete, Gear $gear = null)
    {
        $this->id = $activityResource->id;
        $this->athlete = $athlete;

        $this->update($activityResource, $gear);
    }

    public function update(\stdClass $activityResource, Gear $gear = null): self
    {
        $this->setDetailed($activityResource->resource_state);

        $this->type = $activityResource->type;
        $this->name = $activityResource->name;
        $this->distance = $activityResource->distance;
        $this->elevationGain = $activityResource->total_elevation_gain;
        $this->movingTime = $activityResource->moving_time;
        $this->private = $activityResource->private;
        $this->commute = $activityResource->commute;
        $this->startDate = new DateTimeImmutable($activityResource->start_date);
        $this->utcOffset = intval($activityResource->utc_offset);

        if ($this->isDetailed()) {
            $this->gear = $gear;
            $this->description = $activityResource->description !== null && strlen($activityResource->description) > 0 ? $activityResource->description : null;
            $this->deviceName = $activityResource->device_name ?? null;
        }

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAthlete(): Athlete
    {
        return $this->athlete;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDistance(): float
    {
        return $this->distance;
    }

    public function getElevationGain(): float
    {
        return $this->elevationGain;
    }

    public function getMovingTime(): int
    {
        return $this->movingTime;
    }

    public function isPrivate(): bool
    {
        return $this->private;
    }

    public function isCommute(): bool
    {
        return $this->commute;
    }

    public function getStartDate(): DateTimeInterface
    {
        return $this->startDate;
    }

    public function getUtcOffset(): int
    {
        return $this->utcOffset;
    }

    public function getGear(): ?Gear
    {
        return $this->gear;
    }

    public function getDescription(): ?string
    {
        return $this->name;
    }

    public function getDeviceName(): ?string
    {
        return $this->deviceName;
    }
}
