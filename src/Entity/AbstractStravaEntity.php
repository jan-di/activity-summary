<?php

declare(strict_types=1);

namespace App\Entity;

use App\Service\StravaApi;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
 * @ORM\MappedSuperclass
 */
abstract class AbstractStravaEntity
{
    /**
     * @ORM\Column(name="detailed", type="boolean")
     */
    private bool $detailed;

    public function setDetailed(int $resourceState): static
    {
        if ($resourceState < StravaApi::RESOURCE_STATE_SUMMARY
            || $resourceState > StravaApi::RESOURCE_STATE_DETAILED) {
            throw new InvalidArgumentException('Resource state must be summary or detailed!');
        }

        $this->detailed = $resourceState === StravaApi::RESOURCE_STATE_DETAILED;

        return $this;
    }

    public function isDetailed(): bool
    {
        return $this->detailed;
    }
}
