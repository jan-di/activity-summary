FROM php:8.0-apache

# install packages via apt-get
RUN set -eux; \
  apt-get update; \
  apt-get install -y --no-install-recommends \
        busybox-static \
        netcat \
        unzip \
        git \
  ; \
  rm -rf /var/lib/apt/lists/*

# install php extensions
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN set -eux; \
    install-php-extensions \
        opcache \
        pdo_mysql \
        sockets \
    ; \
    rm /usr/local/bin/install-php-extensions

# change apache config
RUN set -eux; \
    a2enmod rewrite; \
    sed -ri -e 's!/var/www/html!/var/www/html/public!g' /etc/apache2/sites-available/*.conf

# change php config
RUN set -eux; \
    mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
COPY .docker/php.ini $PHP_INI_DIR/conf.d/custom.ini

# move to apache root
WORKDIR /var/www/html

# install composer dependencies
COPY build/composer.json build/composer.lock ./
COPY --from=composer /usr/bin/composer /usr/local/bin/
RUN set -eux; \
    composer install --no-dev --no-progress

# configure cronjobs
COPY .docker/cron/crontab /var/spool/cron/crontabs/www-data

# copy application files
COPY build/ ./
RUN set -eux; \
    composer dump-autoload --classmap-authoritative; \
    rm /usr/local/bin/composer

# create directories to prevent permission issues
RUN set -eux; \
    mkdir -p var/cache var/log; \
    chown -R www-data:www-data var

# additional binaries
ENV PATH="/var/www/html/bin:${PATH}"
COPY .docker/cron/cron.sh bin/cron
RUN set -eux; \
    chmod +x bin/*

# default environment variables
ENV APP_ENV=production

# docker entrypoint
COPY .docker/entrypoint.sh /entrypoint.sh
COPY .docker/wait-for.sh /wait-for.sh
ENTRYPOINT [ "sh", "/entrypoint.sh" ]
CMD ["apache2-foreground"]