<?php

declare(strict_types=1);

use App\Application\Bootstrap\TwigFactory;
use Twig\Environment;
use function DI\autowire;
use function DI\factory;
use function DI\get;
use function DI\string as str;

return [
    'twig.cache.dir' => str('{app.cache.dir}/twig'),
    'twig.template.dir' => str('{app.root}/template'),

    TwigFactory::class => autowire(),

    Environment::class => factory([TwigFactory::class, 'createEnvironment'])
        ->parameter('appEnv', get('app.env'))
        ->parameter('cacheDir', get('twig.cache.dir'))
        ->parameter('templateDir', get('twig.template.dir')),
];
