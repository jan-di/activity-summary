<?php

declare(strict_types=1);

use App\Application\Bootstrap\DoctrineFactory;
use Doctrine\Migrations\DependencyFactory;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use function DI\autowire;
use function DI\create;
use function DI\factory;
use function DI\get;
use function DI\string as str;

return [
    'db.host' => get('config:DB_HOST'),
    'db.port' => get('config:DB_PORT'),
    'db.name' => get('config:DB_NAME'),
    'db.username' => get('config:DB_USERNAME'),
    'db.password' => get('config:DB_PASSWORD'),
    'db.charset' => 'utf8mb4',

    'orm.connection' => [
        'driver' => 'pdo_mysql',
        'host' => get('db.host'),
        'port' => get('db.port'),
        'dbname' => get('db.name'),
        'user' => get('db.username'),
        'password' => get('db.password'),
        'charset' => get('db.charset'),
        'driverOptions' => [
            PDO::ATTR_EMULATE_PREPARES => false,
        ],
    ],

    'orm.entity_dir' => str('{app.root}/src/Entity'),
    'orm.proxy_dir' => str('{app.root}/var/cache/doctrine/proxy'),
    'orm.cache_dir' => str('{app.root}/var/cache/doctrine/cache'),
    'orm.entity.namespace' => str('App\Entity'),

    'migrations.dir' => str('{app.root}/src/Migration'),
    'migrations.namespace' => str('App\Migration'),

    DoctrineFactory::class => autowire(),

    EntityManagerInterface::class => get(EntityManager::class),
    EntityManager::class => factory([DoctrineFactory::class, 'createEntityManager'])
        ->parameter('appEnv', get('app.env'))
        ->parameter('entityDir', get('orm.entity_dir'))
        ->parameter('entityNamespace', get('orm.entity.namespace'))
        ->parameter('proxyDir', get('orm.proxy_dir'))
        ->parameter('cacheDir', get('orm.cache_dir'))
        ->parameter('connection', get('orm.connection')),
    EntityManagerHelper::class => autowire(),

    DependencyFactory::class => factory([DoctrineFactory::class, 'createDependencyFactory'])
        ->parameter('migrationsDir', get('migrations.dir'))
        ->parameter('migrationsNamespace', get('migrations.namespace')),

    'Doctrine\Migrations\Tools\Console\Command\*' => create()->constructor(get(DependencyFactory::class)),
];
