<?php

declare(strict_types=1);

use App\Service\StravaApi;
use App\Service\StravaAuth;
use App\Service\StravaHook;
use function DI\autowire;
use function DI\get;

return [
    'strava.client.id' => get('config:STRAVA_CLIENT_ID'),
    'strava.client.secret' => get('config:STRAVA_CLIENT_SECRET'),
    'strava.webhook.token' => get('config:STRAVA_WEBHOOK_TOKEN'),

    'strava.auth_url' => 'https://www.strava.com/oauth/authorize',
    'strava.deauth_url' => 'https://www.strava.com/oauth/deauthorize',
    'strava.token_url' => 'https://www.strava.com/oauth/token',
    'strava.api_base_url' => 'https://www.strava.com/api/v3',

    'strava.template_url.athlete' => 'https://www.strava.com/athletes/{id}',
    'strava.template_url.activity' => 'https://www.strava.com/activities/{id}',

    StravaAuth::class => autowire()
        ->constructorParameter('clientId', get('strava.client.id'))
        ->constructorParameter('clientSecret', get('strava.client.secret'))
        ->constructorParameter('authUrl', get('strava.auth_url'))
        ->constructorParameter('deauthUrl', get('strava.deauth_url'))
        ->constructorParameter('tokenUrl', get('strava.token_url')),

    StravaHook::class => autowire()
        ->constructorParameter('clientId', get('strava.client.id'))
        ->constructorParameter('clientSecret', get('strava.client.secret'))
        ->constructorParameter('apiBaseUrl', get('strava.api_base_url'))
        ->constructorParameter('webhookToken', get('strava.webhook.token')),

    StravaApi::class => autowire()
        ->constructorParameter('apiBaseUrl', get('strava.api_base_url'))
        ->constructorParameter('logger', get('log.strava_api')),
];
