<?php

declare(strict_types=1);

use PhpAmqpLib\Connection\AMQPLazyConnection;
use function DI\create;
use function DI\get;

return [
    'rabbit.host' => get('config:RABBIT_HOST'),
    'rabbit.port' => get('config:RABBIT_PORT'),
    'rabbit.username' => get('config:RABBIT_USERNAME'),
    'rabbit.password' => get('config:RABBIT_PASSWORD'),
    'rabbit.queue' => 'tasks',

    AMQPLazyConnection::class => create()
        ->constructor(
            get('rabbit.host'),
            get('rabbit.port'),
            get('rabbit.username'),
            get('rabbit.password'),
        ),
];
