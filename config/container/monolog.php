<?php

declare(strict_types=1);

use App\Application\Bootstrap\LoggerFactory;
use function DI\factory;
use function DI\get;
use function DI\string as str;

return [
    'log.error_handler.file_name' => str('{app.log.dir}/crash-dump.log'),
    'log.error_handler' => factory([LoggerFactory::class, 'createErrorHandlerLogger'])
        ->parameter('fileName', get('log.error_handler.file_name')),

    'log.webhook.file_name' => str('{app.log.dir}/webhook.log'),
    'log.webhook' => factory([LoggerFactory::class, 'createWebhookLogger'])
        ->parameter('fileName', get('log.webhook.file_name')),

    'log.strava_api.file_name' => str('{app.log.dir}/strava-api.log'),
    'log.strava_api' => factory([LoggerFactory::class, 'createStravaApiLogger'])
        ->parameter('fileName', get('log.strava_api.file_name')),

    'log.worker.file_name' => str('{app.log.dir}/worker.log'),
    'log.worker' => factory([LoggerFactory::class, 'createWorkerLogger'])
        ->parameter('fileName', get('log.worker.file_name')),
];
