<?php

declare(strict_types=1);

use App\Command\ClearCacheCommand;
use App\Command\WarmupCacheCommand;
use App\Controller\WebhookController;
use App\Display\ActivityTable;
use App\Middleware\ErrorHandlerMiddleware;
use App\Service\MessageQueue;
use App\Service\Response;
use App\Service\Routing;
use Middlewares\RequestHandler;
use Psr\Container\ContainerInterface;
use function DI\autowire;
use function DI\get;
use function DI\string as str;

return [
    'app.root' => realpath(__DIR__.'/../..'),
    'app.env' => get('config:APP_ENV'),
    'app.title' => 'Activity Summary',

    'app.cache.dir' => str('{app.root}/var/cache'),
    'app.config.dir' => str('{app.root}/config'),
    'app.source.dir' => str('{app.root}/src'),
    'app.log.dir' => str('{app.root}/var/log'),

    'self.scheme' => get('config:SELF_SCHEME'),
    'self.host' => get('config:SELF_HOST'),
    'self.base' => get('config:SELF_BASE'),

    'legal.contact' => get('config:LEGAL_CONTACT'),
    'legal.address.line1' => get('config:LEGAL_ADDRESS_LINE1'),
    'legal.address.line2' => get('config:LEGAL_ADDRESS_LINE2'),
    'legal.email' => get('config:LEGAL_EMAIL'),
    'legal.telephone' => get('config:LEGAL_TELEPHONE'),

    // Displays
    ActivityTable::class => autowire()
        ->constructorParameter('activityTemplateUrl', get('strava.template_url.activity')),

    // Controller
    WebhookController::class => autowire()
        ->constructorParameter('logger', get('log.webhook')),

    // Services
    Routing::class => autowire()
        ->constructorParameter('selfScheme', get('self.scheme'))
        ->constructorParameter('selfHost', get('self.host'))
        ->constructorParameter('selfBase', get('self.base')),
    Response::class => autowire(),
    MessageQueue::class => autowire()
        ->constructorParameter('queue', get('rabbit.queue')),

    // Middlewares
    RequestHandler::class => autowire()
        ->constructor(get(ContainerInterface::class)),
    ErrorHandlerMiddleware::class => autowire()
        ->constructorParameter('logger', get('log.error_handler')),

    // Commands
    WarmupCacheCommand::class => autowire()
        ->constructorParameter('appEnv', get('app.env'))
        ->constructorParameter('twigTemplateDir', get('twig.template.dir')),
    ClearCacheCommand::class => autowire()
        ->constructorParameter('globalCacheDir', get('app.cache.dir')),
];
