<?php

declare(strict_types=1);

use Jandi\Config\Entry\IntEntry;
use Jandi\Config\Entry\StringEntry;

return [
    (new StringEntry('APP_ENV'))->setAllowedValues(['development', 'production']),
    (new StringEntry('SELF_SCHEME', 'http'))->setAllowedValues(['http', 'https']),
    (new StringEntry('SELF_HOST', 'localhost')),
    (new StringEntry('SELF_BASE', '')),
    (new StringEntry('DB_HOST', 'localhost')),
    (new IntEntry('DB_PORT', '3306'))->setLowerLimit(0)->setUpperLimit(65535),
    (new StringEntry('DB_NAME')),
    (new StringEntry('DB_USERNAME')),
    (new StringEntry('DB_PASSWORD')),
    (new StringEntry('RABBIT_HOST', 'localhost')),
    (new IntEntry('RABBIT_PORT', '5672'))->setLowerLimit(0)->setUpperLimit(65535),
    (new StringEntry('RABBIT_USERNAME')),
    (new StringEntry('RABBIT_PASSWORD')),
    (new IntEntry('STRAVA_CLIENT_ID')),
    (new StringEntry('STRAVA_CLIENT_SECRET')),
    (new StringEntry('STRAVA_WEBHOOK_TOKEN')),
    (new StringEntry('LEGAL_CONTACT')),
    (new StringEntry('LEGAL_ADDRESS_LINE1', '')),
    (new StringEntry('LEGAL_ADDRESS_LINE2', '')),
    (new StringEntry('LEGAL_EMAIL', '')),
    (new StringEntry('LEGAL_TELEPHONE', '')),
];
