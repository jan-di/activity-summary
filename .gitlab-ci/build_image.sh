#!/bin/sh
set -eux

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

docker pull --quiet $CI_REGISTRY_IMAGE:staging || true
docker build -t $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA \
    --cache-from $CI_REGISTRY_IMAGE:staging .
docker push --quiet $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA