#!/bin/sh
set -eux

composer install --no-progress --no-scripts --ignore-platform-reqs

php vendor/bin/php-cs-fixer fix --dry-run --allow-risky=yes