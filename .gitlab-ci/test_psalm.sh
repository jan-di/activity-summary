#!/bin/sh
set -eux

composer install --no-progress --no-scripts --ignore-platform-reqs

php vendor/bin/psalm