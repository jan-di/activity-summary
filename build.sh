#/bin/sh
set -eux

mkdir -p build
rm -rf build/{*,.*}

# copy application files
cp -r \
    bin \
    config \
    public \
    resources \
    src \
    template \
    .env.example \
    composer.json \
    composer.lock \
    package-lock.json \
    package.json \
    LICENSE \
    README.md \
    webpack.mix.js \
build/
cd build

# build assets
npm ci
npx mix --production
rm -r node_modules resources package-lock.json package.json webpack.mix.js
