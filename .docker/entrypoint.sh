#!/bin/sh
set -e

run_as() {
    su -p www-data -s /bin/sh -c "$1"
}

# change uid/gid of www-data
CURRENT_UID=$(id -u www-data)
CURRENT_GID=$(id -g www-data)
if [ ! -z "$UID" ] && [ "$UID" -ne $CURRENT_UID ]; then
    usermod -u $UID www-data
    find / -xdev -user $CURRENT_UID -exec chown -h www-data {} \;
fi
if [ ! -z "$GID" ] && [ "$GID" -ne $CURRENT_GID ]; then
    groupmod -g $GID www-data
    find / -xdev -group $CURRENT_GID -exec chgrp -h www-data {} \;
fi

# waiting until database is ready
if [ -z "$DB_PORT" ]; then
  DB_PORT=3306
fi;
if [ ! -z "$DB_HOST" ]; then
    echo "Waiting for $DB_HOST:$DB_PORT.."
    sh /wait-for.sh "$DB_HOST:$DB_PORT" -t 20
fi

# waiting until rabbitmq is ready
if [ -z "$RABBIT_PORT" ]; then
  RABBIT_PORT=5672
fi;
if [ ! -z "$RABBIT_HOST" ]; then
    echo "Waiting for $RABBIT_HOST:$RABBIT_PORT.."
    sh /wait-for.sh "$RABBIT_HOST:$RABBIT_PORT" -t 20
fi

# prepare appserver
if [ "$1" = "apache2-foreground" ]; then
    run_as 'php bin/console cache:clear'
    run_as 'php bin/console cache:warmup'
    run_as 'php bin/console migrations:migrate --no-interaction'
fi

# upstream entrypoint (php:*-apache)
exec docker-php-entrypoint $@