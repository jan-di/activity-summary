let mix = require('laravel-mix');

mix.setPublicPath('public')
mix.options({
    fileLoaderDirs: {
        fonts: 'fonts'
    }
});

mix
    .sass('resources/sass/app.scss', 'public/css')  
    .js('resources/js/app.js', 'public/js').extract()
    .browserSync({
        proxy: 'http://appserver',
        socket: {
            domain: 'activity-summary-bs.lndo.site',
            port: 80
        },
        open: false
    });
